package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ThirdBusiness;
import com.name.business.entities.Third;
import com.name.business.representations.complex.ThirdCompleteDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

/**
 * Created by luis on 24/07/17.
 */
@Path("/thirds")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ThirdResource {

    private final ThirdBusiness  thirdBusiness;

    public ThirdResource(ThirdBusiness thirdBusiness) {
        this.thirdBusiness = thirdBusiness;
    }

    /**
     *
     * @param id_third
     * @param id_third_father
     * @param id_person
     * @param id_third_type
     * @param th_type_name
     * @param id_c_th_type
     * @param state_c_th_type
     * @param creation_c_th_type
     * @param modify_c_th_type
     * @param id_cb_third
     * @param id_typedoc_third
     * @param doc_third
     * @param typedoc_third
     * @param name_third
     * @param logo
     * @param id_common_third
     * @param state_third
     * @param creation_third
     * @param modify_third
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    @GET
    @Timed
    public Response getThirdResourceList(@QueryParam("id_third") Long id_third,@QueryParam("id_third_father") Long id_third_father,
                                         @QueryParam("id_person") Long id_person,@QueryParam("id_third_type") Long id_third_type,
                                         @QueryParam("th_type_name")String th_type_name,@QueryParam("id_c_th_type") Long id_c_th_type,
                                         @QueryParam("state_c_th_type") Integer state_c_th_type, @QueryParam("creation_c_th_type") Date creation_c_th_type,
                                         @QueryParam("modify_c_th_type") Date modify_c_th_type,@QueryParam("id_cb_third") Long id_cb_third,
                                         @QueryParam("id_typedoc_third") Long id_typedoc_third,@QueryParam("doc_third")  String doc_third,
                                         @QueryParam("typedoc_third")String typedoc_third,  @QueryParam("name_third") String name_third,
                                         @QueryParam("logo") String logo, @QueryParam("id_common_third")Long id_common_third,
                                         @QueryParam("state_third")Integer state_third,@QueryParam("creation_third") Date creation_third,
                                         @QueryParam("modify_third") Date modify_third,
                                         @QueryParam("first_name") String first_name,
                                         @QueryParam("second_name") String second_name,@QueryParam("first_lastname")  String first_lastname,
                                         @QueryParam("second_lastname") String second_lastname,@QueryParam("birthday")  Date birthday,
                                         @QueryParam("id_dir_person") Long id_dir_person,@QueryParam("id_cbi_person") Long id_cbi_person,
                                         @QueryParam("id_doctype_person") Long id_doctype_person,@QueryParam("doc_person")  String doc_person,
                                         @QueryParam("typedoc_person") Long typedoc_person,@QueryParam("fullname_person")  String fullname_person,
                                         @QueryParam("img_person") String img_person,
                                         @QueryParam("id_c_th_person") Long id_c_th_person,@QueryParam("state_person")  Integer state_person,
                                         @QueryParam("creation_person") Date creation_person,@QueryParam("modify_person")  Date modify_person){
        Response response;

        Either<IException, List<Third>> allViewOffertsEither = thirdBusiness.getThird(id_third, id_third_father,id_person, id_third_type,  th_type_name,  id_c_th_type,
                state_c_th_type,  creation_c_th_type, modify_c_th_type,
                id_cb_third, id_typedoc_third,  doc_third, typedoc_third,  name_third,
                logo, id_common_third,  state_third,  creation_third,  modify_third,first_name,
                second_name,first_lastname,
                second_lastname, birthday,
                id_dir_person, id_cbi_person,
                id_doctype_person,doc_person,
                typedoc_person, fullname_person,
                img_person,
                id_c_th_person, state_person,
                creation_person,modify_person);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param thirdCompleteDTO
     * @return
     */
    @POST
    @Timed
    public Response postThirdResource(ThirdCompleteDTO thirdCompleteDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither =thirdBusiness.createThird(thirdCompleteDTO);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }

        return response;
    }

    /**
     *
     * @param id_third
     * @param thirdDTO
     * @return
     */
    @Path("/{id_third}")
    @PUT
    @Timed
    public Response updateThirdResource(@PathParam("id_third") Long id_third, ThirdCompleteDTO thirdDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdBusiness.updateThird(id_third, thirdDTO);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_third
     * @return
     */
    @Path("/{id_third}")
    @DELETE
    @Timed
    public Response deleteThirdResource(@PathParam("id_third") Long id_third){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdBusiness.deleteThird(id_third);
        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
