package com.name.business.resources;



import com.name.business.businesses.AplicacionBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.entities.Token;
import com.name.business.representations.AplicacionDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;



import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/apps")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AplicacionResource {
    AplicacionBusiness aplicacionBusiness;

    public AplicacionResource(AplicacionBusiness aplicacionBusiness) {
        this.aplicacionBusiness = aplicacionBusiness;
    }

    @GET
    public Response obtenerListaAplicaciones(@Context Aplicacion aplicacion, @QueryParam("id_aplicacion")Long id_applicacion,
                                             @QueryParam("id_usuario_app")Long id_usuario_app,
                                             @QueryParam("nombre") String nombre,
                                             @QueryParam("descripcion") String descripcion, @QueryParam("version") String version,
                                             @QueryParam("fecha_creacion") String fecha_creacion, @QueryParam("fecha_actualizacion") String fecha_actualizacion,
                                             @QueryParam("key_aplicacion") String key_aplicacion, @QueryParam("key_servidor") String key_servidor){
        Response response;

        System.out.println("\n\n\n\n\n\n\n\n");
        System.out.println(" TOKEN "+aplicacion);
        System.out.println("\n\n\n\n\n\n\n\n");

        Either<IException, List<Aplicacion>> allViewOffertsEither = aplicacionBusiness.obtenerAplicaciones(id_applicacion,id_usuario_app, nombre, descripcion, version, fecha_creacion, fecha_actualizacion, key_aplicacion, key_servidor);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearOferta( @QueryParam("id_user")Long id_user, AplicacionDTO app) {
        Either<IException, Long> offerEither = aplicacionBusiness.crearAplicacion(id_user, app);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @PUT
    @Path("{id_app}")
    public Response modificarAplicacion(@Context Aplicacion aplicacion,@PathParam("id_app") Long id_app ,@QueryParam("id_user")Long id_user, AplicacionDTO appDTO) {
        Either<IException, Long> offerEither = aplicacionBusiness.modificarAplicacion(id_app,id_user, appDTO);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }
}
