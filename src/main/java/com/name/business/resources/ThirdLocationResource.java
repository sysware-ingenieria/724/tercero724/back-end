package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ThirdLocationBusiness;
import com.name.business.entities.ThirdLocation;
import com.name.business.entities.ThirdLocationComplete;
import com.name.business.representations.ThirdLocationDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/thirds-locations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ThirdLocationResource {

    public ThirdLocationBusiness thirdLocationBusiness;

    /**
     *
     * @param thirdLocationBusiness
     */
    public ThirdLocationResource(ThirdLocationBusiness thirdLocationBusiness) {
        this.thirdLocationBusiness = thirdLocationBusiness;
    }


    @GET
    @Timed
    public Response getThirdLocationCompleteResource(@QueryParam("id_third_location") Long id_third_location,@QueryParam("id_third") Long id_third,
                                                     @QueryParam("id_location") Long id_location, @QueryParam("id_common_third") Long id_common_third,
                                                     @QueryParam("state") Integer state, @QueryParam("creation_third_location") Date creation_third_location,
                                                     @QueryParam("modify_third_location") Date modify_third_location){
        Response response;
        Either<IException, List<ThirdLocationComplete>> thirdLocationComplete = thirdLocationBusiness.getThirdLocationComplete(id_third_location, id_third, id_location, id_common_third,
                state, creation_third_location,
                modify_third_location);

        if (thirdLocationComplete.isRight()){
            System.out.println(thirdLocationComplete.right().value());
            response=Response.status(Response.Status.OK).entity(thirdLocationComplete.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(thirdLocationComplete);
        }
        return response;
    }


    @POST
    @Timed
    public Response postThirdLocationResource(ThirdLocationDTO thirdLocationDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdLocationBusiness.createThirdLocation(thirdLocationDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }




    @Path("/{id_thirdLocation}")
    @PUT
    @Timed
    public Response updateThirdLocationResource(@PathParam("id_thirdLocation") Long id_thirdLocation, ThirdLocationDTO thirdLocationDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdLocationBusiness.updateLocation(id_thirdLocation, thirdLocationDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id_thirdLocation}")
    @DELETE
    @Timed
    public Response deleteLocationThirdResource(@PathParam("id_thirdLocation") Long id_locationThird){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdLocationBusiness.deleteThirdLocation(id_locationThird);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
