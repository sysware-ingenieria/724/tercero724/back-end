package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.UserThirdBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.entities.UserThird;
import com.name.business.representations.UserThirdDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserThirdResource {
    private UserThirdBusiness userThirdBusiness;

    /**
     *
     * @param userThirdBusiness
     */
    public UserThirdResource(UserThirdBusiness userThirdBusiness) {
        this.userThirdBusiness = userThirdBusiness;

    }

    /**
     *
     * @param id_person
     * @param userThirdDTO
     * @return
     */
    @POST
    @Timed
    public Response postUserThird(@Context Aplicacion aplicacion, @QueryParam("id_person")Long id_person, UserThirdDTO userThirdDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither =userThirdBusiness.createUserThird(id_person, userThirdDTO);

        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_user_third
     * @param UUID
     * @param id_person
     * @param id_common_third
     * @param state
     * @param creation_user
     * @param modify_user
     * @return
     */
    @GET
    @Timed
    public Response getUserThirdResource(@Context Aplicacion aplicacion,@QueryParam("id_user_third")Long id_user_third, @QueryParam("uuid")String UUID,
                                         @QueryParam("id_person") Long id_person,@QueryParam("id_common_third")Long id_common_third,
                                         @QueryParam("state") Integer state, @QueryParam("creation_user")Date creation_user,
                                         @QueryParam("modify_user") Date modify_user){
        Response response;

        Either<IException, List<UserThird>> allViewOffertsEither =userThirdBusiness.getUserThird(id_user_third,UUID, id_person,
                                                                                    id_common_third,state, creation_user, modify_user);

        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_UserThird
     * @param userThirdDTO
     * @return
     */
    @Path("/{id_UserThird}")
    @PUT
    @Timed
    public Response updateUserThirdResource(@Context Aplicacion aplicacion,@PathParam("id_UserThird") Long id_UserThird, UserThirdDTO userThirdDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = userThirdBusiness.updateUserThird(id_UserThird, userThirdDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_UserThird
     * @return
     */
    @Path("/{id_UserThird}")
    @DELETE
    @Timed
    public Response deleteUserThirdResource(@Context Aplicacion aplicacion,@PathParam("id_UserThird") Long id_UserThird){
        Response response;
        Either<IException, Long> allViewOffertsEither = userThirdBusiness.deleteUserThird(id_UserThird);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
