package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ThirdTypeBusiness;
import com.name.business.entities.ThirdType;
import com.name.business.representations.ThirdTypeDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by luis on 21/07/17.
 */
@Path("/thirds-type")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ThirdTypeResource {

    private final ThirdTypeBusiness thirdTypeBusiness;

    /**
     *
     * @param thirdTypeBusiness
     */
    public ThirdTypeResource(ThirdTypeBusiness thirdTypeBusiness) {
        this.thirdTypeBusiness = thirdTypeBusiness;
    }

    /**
     *
     * @param id_third_type
     * @param name
     * @param id_common_third
     * @param state
     * @return
     */
    @GET
    @Timed
    public Response getThirdTypeResourceList(@QueryParam("id_third_type") Long id_third_type, @QueryParam("name")String name, @QueryParam("id_common_third") Long id_common_third, @QueryParam("state") Integer state){
        Response response;

        Either<IException, List<ThirdType>> allViewOffertsEither = thirdTypeBusiness.getThirdType(id_third_type, name,id_common_third,state);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param thirdTypeDTO
     * @return
     */
    @POST
    @Timed
    public Response postThirdTypeResource(ThirdTypeDTO thirdTypeDTO){
        Response response;

        Either<IException, Long> allViewOffertsEither =thirdTypeBusiness.createThirdType(thirdTypeDTO);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idThirdType
     * @param thirdTypeDTO
     * @return
     */
    @Path("/{idThirdType}")
    @PUT
    @Timed
    public Response updateThirdTypeResource(@PathParam("idThirdType") Long idThirdType, ThirdTypeDTO thirdTypeDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdTypeBusiness.updateThirdType(idThirdType, thirdTypeDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idThirdType
     * @return
     */
    @Path("/{idThirdType}")
    @DELETE
    @Timed
    public Response deleteThirdTypeResource(@PathParam("idThirdType") Long idThirdType){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdTypeBusiness.deleteThirdType(idThirdType);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
