package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.LocationBusiness;
import com.name.business.entities.Directory;
import com.name.business.entities.Location;
import com.name.business.entities.LocationComplete;
import com.name.business.representations.LocationDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/locations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LocationResource {

    public LocationBusiness locationBusiness;

    /**
     *
     * @param locationBusiness
     */
    public LocationResource(LocationBusiness locationBusiness) {
        this.locationBusiness = locationBusiness;
    }



    @GET
    @Timed
    public Response getLocationResource(@QueryParam("id_location") Long id_location,@QueryParam("id_third") Long id_third,
                                        @QueryParam("id_directory") Long id_directory,@QueryParam("id_common_third") Long id_common_third,
                                        @QueryParam("state") Integer state,
                                        @QueryParam("creation_location") Date creation_location,@QueryParam("modify_location") Date modify_location){
        Response response;
        Either<IException, List<LocationComplete>> listEither = locationBusiness.getLocationComplete(id_location, id_third, id_directory, id_common_third, state, creation_location, modify_location);

        if (listEither.isRight()){
            System.out.println(listEither.right().value());
            response=Response.status(Response.Status.OK).entity(listEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(listEither);
        }
        return response;
    }

    @POST
    @Timed
    public Response postLocationResource(LocationDTO locationDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = locationBusiness.createLocation(locationDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id_location}")
    @PUT
    @Timed
    public Response updatelocationResource(@PathParam("id_location") Long id_location, LocationDTO locationDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = locationBusiness.updateLocation(id_location, locationDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id_location}")
    @DELETE
    @Timed
    public Response deleteLocationThirdResource(@PathParam("id_location") Long id_location){
        Response response;
        Either<IException, Long> allViewOffertsEither = locationBusiness.deleteLocation(id_location);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
