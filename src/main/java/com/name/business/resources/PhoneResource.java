package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PhoneBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.entities.Phone;
import com.name.business.representations.PhoneDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/phones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PhoneResource {
    private PhoneBusiness phoneBusiness;

    /**
     *
     * @param phoneBusiness
     */
    public PhoneResource(PhoneBusiness phoneBusiness) {
        this.phoneBusiness = phoneBusiness;
    }

    /**
     *
     * @param id_directory
     * @param phoneDTOList
     * @return
     */
    @POST
    @Timed
    public Response postMail(@Context Aplicacion aplicacion, @QueryParam("id_directory")Long id_directory, List<PhoneDTO> phoneDTOList){
        Response response;

        Either<IException, String> mailEither = phoneBusiness.createPhone(id_directory, phoneDTOList);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    /**
     *
     * @param id_mail
     * @param mail
     * @param priority
     * @param id_directory
     * @return
     */
    @GET
    @Timed
    public Response getPersonResource(@Context Aplicacion aplicacion,@QueryParam("id_mail") Long id_mail, @QueryParam("phone")String mail,
                                      @QueryParam("priority") Integer priority,
                                      @QueryParam("id_directory") Long id_directory,
                                      @QueryParam("id_common_third") Long id_common_third,
                                      @QueryParam("state") Integer state,
                                      @QueryParam("creation_phone") Date creation_phone,
                                      @QueryParam("modify_phone") Date modify_phone){
        Response response;

        Either<IException, List<Phone>> allViewOffertsEither =phoneBusiness.getPhones(id_mail,mail,  priority, id_directory,id_common_third,state,creation_phone,modify_phone);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idPhone
     * @param phoneDTO
     * @return
     */
    @Path("/{idPhone}")
    @PUT
    @Timed
    public Response updatePhoneResource(@Context Aplicacion aplicacion,@PathParam("idPhone") Long idPhone, PhoneDTO phoneDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = phoneBusiness.updatePhone(idPhone, phoneDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idPhone
     * @return
     */
    @Path("/{idPhone}")
    @DELETE
    @Timed
    public Response deletePhoneResource(@Context Aplicacion aplicacion,@PathParam("idPhone") Long idPhone){
        Response response;
        Either<IException, Long> allViewOffertsEither = phoneBusiness.deletePhone(idPhone);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
