package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DocumentTypeBusiness;
import com.name.business.entities.DocumentType;
import com.name.business.representations.DocumentTypeDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@Path("/documents-types")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DocumentTypeResource {

    private final DocumentTypeBusiness docTypeBusiness;

    /**
     *
     * @param docTypeBusiness
     */
    public DocumentTypeResource(DocumentTypeBusiness docTypeBusiness) {
        this.docTypeBusiness = docTypeBusiness;
    }

    /**
     *
     * @param id_document_type
     * @param name
     * @return
     */
    @GET
    @Timed
    public Response getDocumentTypeResourceList(@QueryParam("id_document_type") Long id_document_type, @QueryParam("name")String name){
        Response response;

        Either<IException, List<DocumentType>> allViewOffertsEither = docTypeBusiness.getDocumentType(id_document_type, name);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param documentTypeDTO
     * @return
     */
    @POST
    @Timed
    public Response createDocumentTypeResource(DocumentTypeDTO documentTypeDTO){

        Response response;
        Either<IException, Long> allViewOffertsEither = docTypeBusiness.createDocumentType(documentTypeDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_document_type
     * @param documentTypeDTO
     * @return
     */
    @Path("/{id_document_type}")
    @PUT
    @Timed
    public Response updateDocumentTypeResource(@PathParam("id_document_type") Long id_document_type, DocumentTypeDTO documentTypeDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = docTypeBusiness.updateDocumentType(id_document_type, documentTypeDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_document_type
     * @return
     */
    @Path("/{id_document_type}")
    @DELETE
    @Timed
    public Response deleteDocumentTypeResource(@PathParam("id_document_type") Long id_document_type){
        Response response;
        Either<IException, Long> allViewOffertsEither = docTypeBusiness.deleteDocumentType(id_document_type);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
