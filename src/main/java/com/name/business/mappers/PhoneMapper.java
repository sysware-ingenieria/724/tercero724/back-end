package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Phone;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PhoneMapper implements ResultSetMapper<Phone>{


    @Override
    public Phone map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Phone(
        resultSet.getLong("ID_PHONE"),
        resultSet.getLong("ID_DIRECTORY"),
        resultSet.getString("PHONE"),
        resultSet.getInt("PRIORITY"),
                new CommonThird(
                        resultSet.getLong("ID_COMMON_THIRD"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_PHONE"),
                        resultSet.getDate("MODIFY_PHONE")
                )
        );
    }
}
