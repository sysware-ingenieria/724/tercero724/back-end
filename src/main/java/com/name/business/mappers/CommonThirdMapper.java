package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommonThirdMapper implements ResultSetMapper<CommonThird> {
    @Override
    public CommonThird map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new CommonThird(
                resultSet.getLong("ID_COMMON_THIRD"),
                resultSet.getInt("STATE"),
                resultSet.getTimestamp("CREATION_DATE"),
                resultSet.getTimestamp("MODIFY_DATE")
        );
    }
}
