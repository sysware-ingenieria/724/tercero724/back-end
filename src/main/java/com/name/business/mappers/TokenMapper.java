package com.name.business.mappers;


import com.name.business.entities.Token;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis_ on 29/01/2017.
 */
public class TokenMapper implements ResultSetMapper<Token> {
    @Override
    public Token map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Token(
                resultSet.getLong("id_token"),
                resultSet.getString("key_token"),
                resultSet.getLong("id_role"),
                resultSet.getLong("id_person"),
                resultSet.getLong("id_user"),
                resultSet.getString("username"),
                resultSet.getString("full_name"),
                resultSet.getString("last_name")
        );
    }
}
