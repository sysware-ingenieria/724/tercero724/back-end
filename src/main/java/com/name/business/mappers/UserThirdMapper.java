package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.UserThird;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserThirdMapper implements ResultSetMapper<UserThird> {

    @Override
    public UserThird map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new UserThird(
        resultSet.getLong("ID_USER_THIRD"),
        resultSet.getString("UUID"),
                new CommonThird(
                resultSet.getLong("ID_COMMON_THIRD"),
                resultSet.getInt("STATE"),
                resultSet.getDate("CREATION_USER"),
                resultSet.getDate("MODIFY_USER")
        )
        );
    }
}
