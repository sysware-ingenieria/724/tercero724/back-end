package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.ThirdLocation;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThirdLocationMapper implements ResultSetMapper<ThirdLocation>{

    @Override
    public ThirdLocation map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new ThirdLocation(
        resultSet.getLong("id_third_location"),
        resultSet.getLong("id_common_third"),
        resultSet.getLong("id_third"),
        resultSet.getLong("id_location"),
                new CommonThird(
                        resultSet.getLong("ID_COMMON_THIRD"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_TH_LOC"),
                        resultSet.getDate("MODIFY_TH_LOC")
                )

        );
    }
}
