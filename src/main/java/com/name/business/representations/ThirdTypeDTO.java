package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdTypeDTO {

    private String name;
    private Common_ThirdDTO commonThirdDTO;

    @JsonCreator
    public ThirdTypeDTO(@JsonProperty("name") String name,@JsonProperty("state") Common_ThirdDTO commonThirdDTO) {
        this.name = name;
        this.commonThirdDTO = commonThirdDTO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Common_ThirdDTO getCommonThirdDTO() {
        return commonThirdDTO;
    }

    public void setCommonThirdDTO(Common_ThirdDTO commonThirdDTO) {
        this.commonThirdDTO = commonThirdDTO;
    }
}
