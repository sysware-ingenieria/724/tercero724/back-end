package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CommonBasicInfoDTO {

    private String fullname;

    private Long id_document_type;

    private String document_number;

    private String img;

    @JsonCreator
    public CommonBasicInfoDTO(@JsonProperty("fullname")String fullname,@JsonProperty("id_document_type") Long id_document_type,
                              @JsonProperty("document_number") String document_number, @JsonProperty("img") String img) {
        this.fullname = fullname;
        this.id_document_type = id_document_type;
        this.document_number = document_number;
        this.img = img;
    }


    public Long getId_document_type() {
        return id_document_type;
    }

    public void setId_document_type(Long id_document_type) {
        this.id_document_type = id_document_type;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }
}
