package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Common_ThirdDTO {

    private Long state;
    private Date creation_date;
    private Date modify_date;

    @JsonCreator
    public Common_ThirdDTO(
            @JsonProperty("state") Long state,
            @JsonProperty("creation_date") Date creation_date,
            @JsonProperty("modify_date") Date modify_date) {
        this.state = state;
        this.creation_date = creation_date;
        this.modify_date = modify_date;
    }

    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
