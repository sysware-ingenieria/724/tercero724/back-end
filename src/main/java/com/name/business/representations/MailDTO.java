package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by trossky on 14/07/17.
 */
public class MailDTO {

    private String mail;
    private Integer priority;
    private Common_ThirdDTO state;

    @JsonCreator
    public MailDTO(@JsonProperty("mail") String mail,@JsonProperty("priority") Integer priority,
                   @JsonProperty("state") Common_ThirdDTO state) {
        this.mail = mail;
        this.priority = priority;
        this.state=state;
    }


    public Common_ThirdDTO getState() {
        return state;
    }

    public void setState(Common_ThirdDTO state) {
        this.state = state;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }


}
