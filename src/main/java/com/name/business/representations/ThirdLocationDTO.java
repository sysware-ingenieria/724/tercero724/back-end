package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdLocationDTO {
    private  Long id_third;
    private  Long id_location;
    private  Common_ThirdDTO state;

    @JsonCreator
    public ThirdLocationDTO(@JsonProperty("id_third") Long id_third,@JsonProperty("id_location") Long id_location,
                            @JsonProperty("state")  Common_ThirdDTO state) {
        this.id_third = id_third;
        this.id_location = id_location;
        this.state = state;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_location() {
        return id_location;
    }

    public void setId_location(Long id_location) {
        this.id_location = id_location;
    }

    public Common_ThirdDTO getState() {
        return state;
    }

    public void setState(Common_ThirdDTO state) {
        this.state = state;
    }
}
