package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Modela los Datos a Transferir
 * por medio  micro-servicios REST
 * Esto con el fin de solicitar desde el cliente o enviar hacia el cliente
 * desde el servicio REST los formatos de los datos como se deben comunicar, ya sea por
 * medio de un formato JSON.
 * Y así mismo brindar consistencia en los datos que espera el servidor y evitar flujos de
 * información que afecten el rendimiento o consistencia de los datos.
 *
 * */
public class Token_DTO {
    private final String username;
    private final String password;
    private final long id_college;


    @JsonCreator
    public Token_DTO(@JsonProperty("username") String username, @JsonProperty("password") String password, @JsonProperty("id_college") long id_college) {
        this.username = username;
        this.password = password;
        this.id_college=id_college;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getId_college() {
        return id_college;
    }
}
