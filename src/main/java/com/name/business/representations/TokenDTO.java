package com.name.business.representations;

/**
 * Created by luis_ on 29/01/2017.
 */
public class TokenDTO {
    private final String key_TOKEN;
    private final long id_role;
    private final long id_person;
    private final long id_user;
    private final long id_route;
    private final String username;
    private final String fullName;
    private final String lastName;

    public TokenDTO(String key_TOKEN, long id_role, long id_person, long id_user, long id_route, String username, String fullName, String lastName) {
        super();
        this.key_TOKEN = key_TOKEN;
        this.id_role = id_role;
        this.id_person = id_person;
        this.id_user=id_user;
        this.id_route=id_route;
        this.username = username;
        this.fullName = fullName;
        this.lastName = lastName;
    }

    public long getId_user() {
        return id_user;
    }

    public String getKey_TOKEN() {
        return key_TOKEN;
    }

    public long getId_role() {
        return id_role;
    }

    public long getId_person() {
        return id_person;
    }

    public long getId_route() {
        return id_route;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public String getLastName() {
        return lastName;
    }
}
