package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by trossky on 14/07/17.
 */
public class PhoneDTO {

    private String phone;
    private Integer priority;
    private Common_ThirdDTO state;

    @JsonCreator
    public PhoneDTO(@JsonProperty("phone") String phone,@JsonProperty("priority") Integer priority,
                    @JsonProperty("state") Common_ThirdDTO state) {
        this.phone = phone;
        this.priority = priority;
        this.state=state;
    }

    public Common_ThirdDTO getState() {
        return state;
    }

    public void setState(Common_ThirdDTO state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
