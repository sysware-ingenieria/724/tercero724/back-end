package com.name.business;

import com.name.business.DAOs.*;
import com.name.business.businesses.*;
import com.name.business.entities.Aplicacion;
import com.name.business.representations.Token_DTO;
import com.name.business.resources.*;
import com.name.business.utils.security.AuthorizationUri;
import com.name.business.utils.security.TokenInjectorFactory;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.Timer;

import static com.name.business.utils.constans.K.URI_BASE;


public class Third724 extends Application<Third724AppConfiguration> {

    public static void main(String[] args) throws Exception  {
        new Third724().run(args);
    }

    @Override
    public void initialize(Bootstrap<Third724AppConfiguration> bootstrap) {
        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());
    }

    @Override
    public void run(Third724AppConfiguration configuration, Environment environment) throws Exception {

        environment.jersey().setUrlPattern(URI_BASE+"/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        // Se estable el ambiente de coneccion con JDBI con la base de datos de oracle
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "oracle");


        Third724DatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);



        final AplicacionDAO aplicacionDAO= jdbi.onDemand(AplicacionDAO.class);
        final AplicacionBusiness aplicacionBusiness= new AplicacionBusiness(aplicacionDAO);

        final CommonThirdDAO commonThirdDAO= jdbi.onDemand(CommonThirdDAO.class);
        final CommonThirdBusiness commonThirdBusiness= new CommonThirdBusiness(commonThirdDAO);

        final DocumentTypeDAO docTypeDAO = jdbi.onDemand(DocumentTypeDAO.class);
        final DocumentTypeBusiness docTypeBusiness= new DocumentTypeBusiness(docTypeDAO, commonThirdBusiness);


        final ThirdTypeDAO thirdTypeDAO= jdbi.onDemand(ThirdTypeDAO.class);
        final ThirdTypeBusiness thirdTypeBusiness= new ThirdTypeBusiness(thirdTypeDAO,commonThirdBusiness);

        final PhoneDAO phoneDAO = jdbi.onDemand(PhoneDAO.class);
        final PhoneBusiness phoneBusiness =new PhoneBusiness(phoneDAO,commonThirdBusiness);


        final MailDAO mailDAO= jdbi.onDemand(MailDAO.class);
        final MailBusiness mailBusiness= new MailBusiness(mailDAO,commonThirdBusiness);

        final DirectoryDAO directoryDAO= jdbi.onDemand(DirectoryDAO.class);
        final DirectoryBusiness directoryBusiness = new DirectoryBusiness(directoryDAO,commonThirdBusiness,mailBusiness,phoneBusiness);

        final CommonBasicInfoDAO commonBasicInfoDAO =jdbi.onDemand(CommonBasicInfoDAO.class);
        final CommonBasicInfoBusiness commonBasicInfoBusiness= new CommonBasicInfoBusiness(commonBasicInfoDAO);


        final LocationDAO locationDAO = jdbi.onDemand(LocationDAO.class);
        final LocationBusiness locationBusiness= new LocationBusiness(locationDAO,commonThirdBusiness,directoryBusiness);

        final EmployeeDAO employeeDAO= jdbi.onDemand(EmployeeDAO.class);
        final EmployeeBusiness employeeBusiness= new EmployeeBusiness(employeeDAO, commonThirdBusiness);

        final UserThirdDAO userThirdDAO = jdbi.onDemand(UserThirdDAO.class);
        final UserThirdBusiness userThirdBusiness=  new UserThirdBusiness(userThirdDAO,commonThirdBusiness);

        final PersonDAO personDAO = jdbi.onDemand(PersonDAO.class);
        final PersonBusiness  personBusiness= new PersonBusiness(personDAO,commonBasicInfoBusiness,commonThirdBusiness,directoryBusiness,employeeBusiness,userThirdBusiness);

        final ThirdLocationDAO thirdLocationDAO = jdbi.onDemand(ThirdLocationDAO.class);
        final ThirdLocationBusiness thirdLocationBusiness= new ThirdLocationBusiness(thirdLocationDAO,locationBusiness,directoryBusiness,commonThirdBusiness);


        final ThirdDAO thirdDAO = jdbi.onDemand(ThirdDAO.class);
        final ThirdBusiness thirdBusiness= new ThirdBusiness(thirdDAO,personBusiness,commonBasicInfoBusiness,commonThirdBusiness,directoryBusiness,locationBusiness,thirdLocationBusiness);



        final TokenDAO tokenDAO = jdbi.onDemand(TokenDAO.class);
        final TokenBusiness tokenBusiness = new TokenBusiness(tokenDAO,aplicacionDAO);

        // apply security on http request
        addAuthorizationFilterAndProvider(environment,tokenBusiness);


        //environment.jersey().register(new TestResource(datoBusiness));

        // apply security on http request
        // addAuthorizationFilterAndProvider(environment,tokenBusiness);

        /**
         * FCM Logic
        final FCM_DAO fcmDao =jdbi.onDemand(FCM_DAO.class);
        final FCMBusiness fcmBusiness= new FCMBusiness(fcmDao);
        final FCMThread fcmThread= new FCMThread(fcmBusiness);
         */


        environment.jersey().register(new AplicacionResource(aplicacionBusiness));           //servicio para test
        environment.jersey().register(new DocumentTypeResource(docTypeBusiness));
        environment.jersey().register(new ThirdTypeResource(thirdTypeBusiness));
        environment.jersey().register(new ThirdResource(thirdBusiness));
        environment.jersey().register(new PersonResource(personBusiness));

        environment.jersey().register( new EmployeeResource(employeeBusiness) );
        environment.jersey().register( new ThirdTypeResource(thirdTypeBusiness) );
        environment.jersey().register( new UserThirdResource(userThirdBusiness) );
        environment.jersey().register( new MailResource(mailBusiness));
        environment.jersey().register( new PhoneResource(phoneBusiness));
        environment.jersey().register( new DirectoryResource(directoryBusiness));
        environment.jersey().register( new LocationResource(locationBusiness));
        environment.jersey().register( new ThirdLocationResource(thirdLocationBusiness));


        Timer timer = new Timer();
        //timer.scheduleAtFixedRate(fcmThread, 0, 30*1000);   //cada 5 minutos

    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(Third724DatabaseConfiguration third724DatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(third724DatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(third724DatabaseConfiguration.getUrl());
        basicDataSource.setUsername(third724DatabaseConfiguration.getUsername());
        basicDataSource.setPassword(third724DatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }

    private void addAuthorizationFilterAndProvider(Environment environment, TokenBusiness tokenBusiness) {

        AuthorizationUri authorizationUri = new AuthorizationUri(tokenBusiness);
        String urlPattern = (URI_BASE + "/*");
        environment.servlets().addFilter("authorizationFilter", authorizationUri)
                .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, urlPattern);
        AbstractBinder abstractBinder = new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(TokenInjectorFactory.class).to(Aplicacion.class).in(RequestScoped.class);
            }
        };


        environment.jersey().getResourceConfig().register(abstractBinder);
    }




}
