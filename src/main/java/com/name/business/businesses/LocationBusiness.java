package com.name.business.businesses;

import com.name.business.DAOs.LocationDAO;
import com.name.business.entities.Directory;
import com.name.business.entities.Location;
import com.name.business.entities.LocationComplete;
import com.name.business.representations.LocationDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by trossky on 14/07/17.
 */
public class LocationBusiness {

    private LocationDAO locationDAO;
    private CommonThirdBusiness commonThirdBusiness;
    private DirectoryBusiness directoryBusiness;

    /**
     * @param locationDAO
     * @param commonThirdBusiness
     * @param directoryBusiness
     */
    public LocationBusiness(LocationDAO locationDAO, CommonThirdBusiness commonThirdBusiness, DirectoryBusiness directoryBusiness) {
        this.locationDAO = locationDAO;
        this.commonThirdBusiness = commonThirdBusiness;
        this.directoryBusiness = directoryBusiness;
    }

    /**
     * @param locationDTO
     * @return
     */
    public Either<IException, Long> createLocation(LocationDTO locationDTO) {
        List<String> msn = new ArrayList<>();
        Long id_directory = null;
        Long id_common_third = null;
        System.out.println(locationDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Location  ||||||||||||||||| ");
            if (locationDTO != null) {
                if (locationDTO.getCommon_thirdDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(locationDTO.getCommon_thirdDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_third = commonThirdEither.right().value();
                    }
                }
                locationDAO.create(id_common_third, locationDTO);

                return Either.right(Long.valueOf(locationDAO.getPkLast()));
            } else {
                msn.add("It does not recognize Location, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_location
     * @param id_third
     * @param id_directory
     * @param id_common_third
     * @param state
     * @param creation_location
     * @param modify_location
     * @return
     */
    public Either<IException, List<Directory>> getLocation(Long id_location, Long id_third, Long id_directory,
                                                           Long id_common_third, Integer state,
                                                           Date creation_location, Date modify_location) {
        List<String> msn = new ArrayList<>();
        List<Directory> directoryList = new ArrayList<>();
        try {

            List<Location> read = locationDAO.read(formatoLongSql(id_location), formatoLongSql(id_third), formatoLongSql(id_directory), formatoLongSql(id_common_third),
                    formatoIntegerSql(state), formatoDateSql(creation_location), formatoDateSql(modify_location));
            if (read.size() > 0) {
                for (Location location : read) {
                    Either<IException, List<Directory>> directoryEither = directoryBusiness.getDirectory(location.getId_directory(), null, null, null,
                            null, null, null, null, null);
                    if (directoryEither.isRight()) {
                        directoryList.addAll(directoryEither.right().value());

                    }
                }
                return Either.right(directoryList);
            } else {
                msn.add("It does not recognize location, probably it has bad format");
            }

            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }



    public Either<IException, List<LocationComplete>> getLocationComplete(Long id_location, Long id_third, Long id_directory,
                                                                  Long id_common_third, Integer state,
                                                                  Date creation_location, Date modify_location) {
        List<String> msn = new ArrayList<>();
        List<LocationComplete> locationList = new ArrayList<>();
        LocationComplete locationComplete =null;
        try {

            List<Location> read = locationDAO.read(formatoLongSql(id_location), formatoLongSql(id_third), formatoLongSql(id_directory), formatoLongSql(id_common_third),
                    formatoIntegerSql(state), formatoDateSql(creation_location), formatoDateSql(modify_location));
            if (read.size() > 0) {
                for (Location location : read) {
                    Either<IException, List<Directory>> directoryEither = directoryBusiness.getDirectory(location.getId_directory(), null, null, null,
                            null, null, null, null, null);
                    if (directoryEither.isRight()) {
                        List<Directory> directories = directoryEither.right().value();

                        locationComplete = new LocationComplete(location.getId_location(),location.getId_third(),directories,location.getState());

                        locationList.add(locationComplete);

                    }
                }
                return Either.right(locationList);
            } else {
                msn.add("It does not recognize location, probably it has bad format");
            }

            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }


    /**
     * 
     * @param idLocation
     * @param locationDTO
     * @return
     */
    public Either<IException, Long> updateLocation(Long idLocation, LocationDTO locationDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE location ||||||||||||||||| ");
            if (idLocation != null && idLocation > 0) {

                //TO DO validate the existence of locations with that id
                Location actualLocation = locationDAO.read(formatoLongSql(idLocation), null, null, null, null, null, null).get(0);

                // Validate the inserted idThird is valid, if null, set the name to the actual in the database, if not it is formatted to sql
                if (Long.valueOf(locationDTO.getId_third()) == null || locationDTO.getId_third() == 0) {
                    locationDTO.setId_third(actualLocation.getId_third());
                } else {
                    locationDTO.setId_third(formatoLongSql(locationDTO.getId_third()));
                }

                // Validate inserted iddirectory  is valid, if null, set the name to the actual in the database, if not it is formatted to sql
                if (Long.valueOf(locationDTO.getId_directory()) == null || locationDTO.getId_directory() == 0) {
                    locationDTO.setId_directory(actualLocation.getId_directory());
                } else {
                    locationDTO.setId_directory(formatoLongSql(locationDTO.getId_directory()));
                }

                locationDAO.update(formatoLongSql(idLocation), locationDTO);
                Long idCommon = locationDAO.delete(formatoLongSql(idLocation)).get(0);
                commonThirdBusiness.updateCommonThird(idCommon, locationDTO.getCommon_thirdDTO());
                return Either.right(idLocation);
            } else {
                msn.add("It does not recognize ID location, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idLocation
     * @return
     */
    public Either<IException, Long> deleteLocation(Long idLocation) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idLocation != null && idLocation > 0) {

                Long idCommon = locationDAO.delete(formatoLongSql(idLocation)).get(0);
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idLocation);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
