package com.name.business.businesses;

import com.name.business.DAOs.ThirdTypeDAO;
import com.name.business.entities.ThirdType;
import com.name.business.representations.ThirdTypeDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdTypeBusiness {
    private ThirdTypeDAO thirdTypeDAO;
    private CommonThirdBusiness commonThirdBusiness;

    /**
     *
     * @param thirdTypeDAO
     * @param commonThirdBusiness
     */
    public ThirdTypeBusiness(ThirdTypeDAO thirdTypeDAO, CommonThirdBusiness commonThirdBusiness) {
        this.thirdTypeDAO = thirdTypeDAO;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    /**
     *
     * @param id_thrid_type
     * @param name
     * @param id_document_type
     * @param state
     * @return
     */
    public Either<IException, List<ThirdType>> getThirdType(Long id_thrid_type, String name, Long id_document_type, Integer state) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Third Type ||||||||||||||||| ");
            if (null == null) {
                List<ThirdType> read = new ArrayList<>();
                return Either.right(thirdTypeDAO.read(formatoLongSql(id_thrid_type), formatoLIKESql(name), formatoLongSql(id_document_type), formatoIntegerSql(state)));

            } else {
                msn.add("It does not recognize Third Type, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param thirdTypeDTO
     * @return
     */
    public Either<IException, Long> createThirdType(ThirdTypeDTO thirdTypeDTO) {
        List<String> msn = new ArrayList<>();

        Long id_common_third=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation User Third ||||||||||||||||| ");
            if (thirdTypeDTO != null) {

                if (thirdTypeDTO.getCommonThirdDTO()!=null){
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(thirdTypeDTO.getCommonThirdDTO());

                    if (commonThirdEither.isRight()){
                        id_common_third=commonThirdEither.right().value();
                    }
                }

                thirdTypeDAO.create(thirdTypeDTO,id_common_third);

                return Either.right(thirdTypeDAO.getPkLast());

            } else {
                msn.add("It does not recognize User Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> updateThirdType(Long idThirdType, ThirdTypeDTO thirdTypeDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE DocumentType ||||||||||||||||| ");
            if (idThirdType != null && idThirdType > 0) {

                ThirdType actualThirdType = thirdTypeDAO.read(formatoLongSql(idThirdType), null, null, null).get(0);

                // Validate name inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (thirdTypeDTO.getName() == null || thirdTypeDTO.getName().equals("")) {
                    thirdTypeDTO.setName(actualThirdType.getName());
                } else {
                    thirdTypeDTO.setName(formatoStringSql(thirdTypeDTO.getName()));
                }

                thirdTypeDAO.update(formatoLongSql(idThirdType), thirdTypeDTO);
                Long idCommon = thirdTypeDAO.delete(formatoLongSql(idThirdType)).get(0);
                commonThirdBusiness.updateCommonThird(idCommon, thirdTypeDTO.getCommonThirdDTO());


                return Either.right(idThirdType);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param idThirdType
     * @return
     */
    public Either<IException, Long> deleteThirdType(Long idThirdType) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idThirdType != null && idThirdType > 0) {

                Long idCommon = thirdTypeDAO.delete(formatoLongSql(idThirdType)).get(0);

                // TO DO when thirdLocation has common third
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idThirdType);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
