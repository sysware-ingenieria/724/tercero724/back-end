package com.name.business.businesses;

import com.name.business.DAOs.DocumentTypeDAO;
import com.name.business.entities.DocumentType;
import com.name.business.representations.DocumentTypeDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by Niki on 17/07/2017.
 */
public class DocumentTypeBusiness {

    private DocumentTypeDAO docTypeDao;
    private CommonThirdBusiness commonThirdBusiness;

    /**
     * @param docTypeDao
     * @param commonThirdBusiness
     */
    public DocumentTypeBusiness(DocumentTypeDAO docTypeDao, CommonThirdBusiness commonThirdBusiness) {
        this.docTypeDao = docTypeDao;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    /**
     * @param id_document_type
     * @param name
     * @return
     */
    public Either<IException, List<DocumentType>> getDocumentType(Long id_document_type, String name) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Document_Type |||||||||||||||||");
            if (null == null) {
                List<DocumentType> read = docTypeDao.read(formatoLongSql(id_document_type), formatoLIKESql(name));
                return Either.right(read);

            } else {
                msn.add("It does not recognize docType, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param docTypeDto
     * @return
     */
    public Either<IException, Long> createDocumentType(DocumentTypeDTO docTypeDto) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting Insert Document_Type |||||||||||||||||");
            if (!docTypeDto.getName().equals(null)) {

                Either<IException, Long> createCommonThird = commonThirdBusiness.createCommonThird(docTypeDto.getCommon_ThirdDTO());
                if (createCommonThird.isRight()) {
                    docTypeDao.create(docTypeDto, createCommonThird.right().value());
                    return Either.right(docTypeDao.getPkLast());
                } else {
                    msn.add("It cannot create state information for the new Document Type");
                }
            } else {
                msn.add("It does not recognize document type name, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_DocumentType
     * @param documentTypeDTO
     * @return
     */
    public Either<IException, Long> updateDocumentType(Long id_DocumentType, DocumentTypeDTO documentTypeDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE DocumentType ||||||||||||||||| ");
            if (id_DocumentType != null && id_DocumentType > 0) {

                // Validate name inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (documentTypeDTO.getName() == null || documentTypeDTO.getName().equals("")) {
                    documentTypeDTO.setName(docTypeDao.read(formatoLongSql(id_DocumentType), null).get(0).getName());
                } else {
                    documentTypeDTO.setName(formatoStringSql(documentTypeDTO.getName()));
                }

                docTypeDao.update(formatoLongSql(id_DocumentType), documentTypeDTO);
                Long idCommon = docTypeDao.delete(formatoLongSql(id_DocumentType)).get(0);
                commonThirdBusiness.updateCommonThird(idCommon, documentTypeDTO.getCommon_ThirdDTO());
                return Either.right(id_DocumentType);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_DocumentType
     * @return
     */
    public Either<IException, Long> deleteDocumentType(Long id_DocumentType) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (id_DocumentType != null && id_DocumentType > 0) {

                Long idCommon = docTypeDao.delete(formatoLongSql(id_DocumentType)).get(0);
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(id_DocumentType);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}