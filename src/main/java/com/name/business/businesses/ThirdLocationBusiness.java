package com.name.business.businesses;

import com.name.business.DAOs.ThirdLocationDAO;
import com.name.business.entities.CommonThird;
import com.name.business.entities.Directory;
import com.name.business.entities.ThirdLocation;
import com.name.business.entities.ThirdLocationComplete;
import com.name.business.representations.ThirdLocationDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdLocationBusiness {

    private ThirdLocationDAO thirdLocationDAO;
    private LocationBusiness locationBusiness;
    private DirectoryBusiness directoryBusiness;
    private CommonThirdBusiness commonThirdBusiness;

    /**
     *
     * @param thirdLocationDAO
     * @param locationBusiness
     * @param directoryBusiness
     */
    public ThirdLocationBusiness(ThirdLocationDAO thirdLocationDAO, LocationBusiness locationBusiness, DirectoryBusiness directoryBusiness, CommonThirdBusiness commonThirdBusiness) {
        this.thirdLocationDAO = thirdLocationDAO;
        this.locationBusiness = locationBusiness;
        this.directoryBusiness = directoryBusiness;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    /**
     *
     * @param id_third_location
     * @param id_third
     * @param id_location
     * @param id_common_third
     * @param state
     * @param creation_third_location
     * @param modify_third_location
     * @return
     */
    public Either<IException, List<Directory>> getThirdLocation(Long id_third_location, Long id_third,
                                                                Long id_location, Long id_common_third,
                                                                Integer state, Date creation_third_location,
                                                                Date modify_third_location) {
        List<String> msn = new ArrayList<>();
        List<Directory> directoryList=new ArrayList<>();
        try{

            List<ThirdLocation> read = thirdLocationDAO.read(formatoLongSql(id_third_location),formatoLongSql(id_third),
                    formatoLongSql(id_location),formatoLongSql(id_common_third),formatoIntegerSql(state),
                    formatoDateSql(creation_third_location),formatoDateSql(modify_third_location));
            if (read.size()>0){
                for (ThirdLocation thirdLocation: read) {
                    Either<IException, List<Directory>> locationEither = locationBusiness.getLocation(thirdLocation.getId_location(), null, null, null, null, null, null);
                    if (locationEither.isRight()){
                        directoryList.addAll(locationEither.right().value());
                    }
                }
                return Either.right(directoryList);
            } else{
                msn.add("It does not recognize location, probably it has bad format");


            }

            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public Either<IException, List<ThirdLocationComplete>> getThirdLocationComplete(Long id_third_location, Long id_third,
                                                                                    Long id_location, Long id_common_third,
                                                                                    Integer state, Date creation_third_location,
                                                                                    Date modify_third_location) {
        List<String> msn = new ArrayList<>();
        List<Directory> directoryList=new ArrayList<>();
        List<ThirdLocationComplete> thirdLocationCompleteList= new ArrayList<>();
        ThirdLocationComplete thirdLocationComplete=null;
        try{

            List<ThirdLocation> read = thirdLocationDAO.read(formatoLongSql(id_third_location),formatoLongSql(id_third),
                    formatoLongSql(id_location),formatoLongSql(id_common_third),formatoIntegerSql(state),
                    formatoDateSql(creation_third_location),formatoDateSql(modify_third_location));
            if (read.size()>0){
                for (ThirdLocation thirdLocation: read) {
                    Either<IException, List<Directory>> locationEither = locationBusiness.getLocation(thirdLocation.getId_location(), null, null, null, null, null, null);
                    if (locationEither.isRight()){

                        directoryList.addAll(locationEither.right().value());

                        thirdLocationComplete= new ThirdLocationComplete(thirdLocation.getId_third_location(),thirdLocation.getId_third(),
                                thirdLocation.getId_location(),thirdLocation.getState(),directoryList);

                        thirdLocationCompleteList.add(thirdLocationComplete);
                    }
                }
                return Either.right(thirdLocationCompleteList);
            } else{
                msn.add("It does not recognize location, probably it has bad format");


            }

            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public Either<IException, Long> updateLocation(Long idThirdLocation, ThirdLocationDTO thirdLocationDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE DocumentType ||||||||||||||||| ");
            if (idThirdLocation != null && idThirdLocation > 0) {

                ThirdLocation actualThirdLocation = thirdLocationDAO.read(idThirdLocation, null, null, null, null, null, null).get(0);

                // Validate id location inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (thirdLocationDTO.getId_location() == null || thirdLocationDTO.getId_location() <= 0 ) {
                    thirdLocationDTO.setId_location(actualThirdLocation.getId_location());
                } else {
                    thirdLocationDTO.setId_location(formatoLongSql(thirdLocationDTO.getId_location()));
                }

                // Validate id third inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (thirdLocationDTO.getId_third() == null || thirdLocationDTO.getId_third() <= 0) {
                    thirdLocationDTO.setId_third(actualThirdLocation.getId_third());
                } else {
                    thirdLocationDTO.setId_third(formatoLongSql(thirdLocationDTO.getId_third()));
                }



                thirdLocationDAO.update(formatoLongSql(idThirdLocation), thirdLocationDTO);
                Long idCommon = thirdLocationDAO.delete(formatoLongSql(idThirdLocation)).get(0);

                //TO DO when common third business has common third
                commonThirdBusiness.updateCommonThird(idCommon, thirdLocationDTO.getState());
                return Either.right(idThirdLocation);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idThirdLocation
     * @return
     */
    public Either<IException, Long> deleteThirdLocation(Long idThirdLocation) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idThirdLocation != null && idThirdLocation > 0) {

                Long idCommon = thirdLocationDAO.delete(formatoLongSql(idThirdLocation)).get(0);

                // TO DO when thidLocation has common third
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idThirdLocation);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> createThirdLocation(ThirdLocationDTO thirdLocationDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common_third = null;
        System.out.println(thirdLocationDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Third Location  ||||||||||||||||| ");
            if (thirdLocationDTO != null) {
                if (thirdLocationDTO.getState() != null) {
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(thirdLocationDTO.getState());
                    if (commonThirdEither.isRight()) {
                        id_common_third = commonThirdEither.right().value();
                    }
                }
                thirdLocationDAO.create(id_common_third, thirdLocationDTO);

                return Either.right(thirdLocationDAO.getPkLast());
            } else {
                msn.add("It does not recognize Location, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }
}
