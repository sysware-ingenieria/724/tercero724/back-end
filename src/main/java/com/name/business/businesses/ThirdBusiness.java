package com.name.business.businesses;

import com.name.business.DAOs.ThirdDAO;
import com.name.business.entities.Directory;
import com.name.business.entities.Person;
import com.name.business.entities.Third;
import com.name.business.entities.ThirdLocation;
import com.name.business.representations.*;
import com.name.business.representations.complex.ThirdCompleteDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;


public class ThirdBusiness {

    private ThirdDAO thirdDAO;
    private PersonBusiness personBusiness;
    private CommonBasicInfoBusiness commonBasicInfoBusiness;
    private CommonThirdBusiness commonThirdBusiness;
    private DirectoryBusiness directoryBusiness;
    private LocationBusiness locationBusiness;
    private ThirdLocationBusiness thirdLocationBusiness;

    /**
     *
     * @param thirdDAO
     * @param personBusiness
     * @param commonBasicInfoBusiness
     * @param commonThirdBusiness
     * @param directoryBusiness
     * @param locationBusiness
     * @param thirdLocationBusiness
     */
    public ThirdBusiness(ThirdDAO thirdDAO, PersonBusiness personBusiness, CommonBasicInfoBusiness commonBasicInfoBusiness, CommonThirdBusiness commonThirdBusiness, DirectoryBusiness directoryBusiness, LocationBusiness locationBusiness, ThirdLocationBusiness thirdLocationBusiness) {
        this.thirdDAO = thirdDAO;
        this.personBusiness = personBusiness;
        this.commonBasicInfoBusiness = commonBasicInfoBusiness;
        this.commonThirdBusiness = commonThirdBusiness;
        this.directoryBusiness = directoryBusiness;
        this.locationBusiness = locationBusiness;
        this.thirdLocationBusiness = thirdLocationBusiness;
    }

    /**
     *
     * @param id_third
     * @param id_third_father
     * @param id_person
     * @param id_third_type
     * @param th_type_name
     * @param id_c_th_type
     * @param state_c_th_type
     * @param creation_c_th_type
     * @param modify_c_th_type
     * @param id_cb_third
     * @param id_typedoc_third
     * @param doc_third
     * @param typedoc_third
     * @param name_third
     * @param logo
     * @param id_common_third
     * @param state_third
     * @param creation_third
     * @param modify_third
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    public Either<IException, List<Third>> getThird(Long id_third, Long id_third_father,Long id_person, Long id_third_type, String th_type_name, Long id_c_th_type,
                                                   Integer state_c_th_type, Date creation_c_th_type, Date modify_c_th_type,
                                                    Long id_cb_third, Long id_typedoc_third,  String doc_third,String typedoc_third,  String name_third,
                                                    String logo, Long id_common_third, Integer state_third, Date creation_third, Date modify_third,
                                                    String first_name,
                                                    String second_name, String first_lastname,
                                                    String second_lastname, Date birthday,
                                                    Long id_dir_person,  Long id_cbi_person,
                                                    Long id_doctype_person, String doc_person,
                                                    Long typedoc_person, String fullname_person,
                                                    String img_person,
                                                    Long id_c_th_person, Integer state_person,
                                                    Date creation_person, Date modify_person) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Third ||||||||||||||||| ");
            if (null == null) {
                List<Third> read = thirdDAO.read(formatoLongSql(id_third),formatoLongSql(id_third_father),formatoLongSql(id_person),formatoLongSql(id_third_type), formatoLIKESql(th_type_name),formatoLongSql(id_c_th_type), formatoIntegerSql(state_c_th_type),
                        formatoDateSql(creation_c_th_type),formatoDateSql(modify_c_th_type),formatoLongSql(id_cb_third),formatoLongSql(id_typedoc_third),formatoStringSql(doc_third),formatoLIKESql(typedoc_third),formatoLIKESql(name_third),
                        formatoStringSql(logo),formatoLongSql(id_common_third),formatoIntegerSql(state_third),formatoDateSql(creation_third),formatoDateSql(modify_third));

                Either<IException, List<Third>> loadThirdPerson=loadThirdPerson(read,first_name,second_name,first_lastname,second_lastname,birthday,id_dir_person,id_cbi_person,id_doctype_person,doc_person,typedoc_person,
                        fullname_person,img_person,id_c_th_person,state_person,creation_person,modify_person);
                return Either.right(read);

            } else {
                msn.add("It does not recognize Third , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param read
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    private Either<IException, List<Third>> loadThirdPerson(List<Third> read,String first_name,
                                                            String second_name, String first_lastname,
                                                            String second_lastname, Date birthday,
                                                            Long id_dir_person,  Long id_cbi_person,
                                                            Long id_doctype_person, String doc_person,
                                                            Long typedoc_person, String fullname_person,
                                                            String img_person,
                                                            Long id_c_th_person, Integer state_person,
                                                            Date creation_person, Date modify_person) {
        List<Directory> directoryList= new ArrayList<>();
        for (int i = 0; i < read.size(); i++) {
            Long id_person = (Long) read.get(i).getProfile();
            Long id_third_father = (Long) read.get(i).getId_third_father();
            if (id_person>0){
                System.out.println("Load data person");
                Either<IException, List<Person>> personEither = personBusiness.getPerson(formatoLongSql(id_person),
                        first_name,second_name,first_lastname,second_lastname,birthday,id_dir_person,id_cbi_person,id_doctype_person,
                        doc_person,typedoc_person,fullname_person,img_person,id_c_th_person,state_person,creation_person,modify_person);
                if (personEither.isRight()){

                    List<Person> personList = personEither.right().value();
                    if (personList.size()>0){
                        read.get(i).setProfile(personList.get(0));
                    }

                }
            }

            if (id_third_father<=0){
                Either<IException, List<Directory>> locationEither = locationBusiness.getLocation(null,
                        read.get(i).getId_third(), null, null, null, null, null);
                if (locationEither.isRight()){
                    directoryList = locationEither.right().value();
                    read.get(i).setDirectory(directoryList);
                }

            }else{
                //TODO third location
                Either<IException, List<Directory>> thirdLocationEither = thirdLocationBusiness.getThirdLocation(null,
                        read.get(i).getId_third(), null, null, null, null, null);
                if (thirdLocationEither.isRight()){

                    directoryList = thirdLocationEither.right().value();
                    read.get(i).setDirectory(directoryList);

                }
            }
        }
        return Either.right(read);
    }

    /**
     *
     * @param thirdCompleteDTO
     * @return
     */
    public Either<IException, Long> createThird(ThirdCompleteDTO thirdCompleteDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common_basicinfo=null;
        Long id_common_third=null;
        Long id_person=null;
        Long id_directory=null;
        Long id_third=null;
        System.out.println(thirdDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Third  ||||||||||||||||| ");
            if (thirdCompleteDTO != null) {




                // TODO regiter basic info data
                if (thirdCompleteDTO.getBasicInfoDTO() != null) {

                    Either<IException, Long> commonBasicInfoEither = commonBasicInfoBusiness.createCommonBasicInfo(thirdCompleteDTO.getBasicInfoDTO());

                    if (commonBasicInfoEither.isRight()){
                        id_common_basicinfo=commonBasicInfoEither.right().value();
                    }
                }



                // TODO regiter common third
                if (thirdCompleteDTO.getCommon_thirdDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(thirdCompleteDTO.getCommon_thirdDTO());
                    if (commonThirdEither.isRight()){
                        id_common_third=commonThirdEither.right().value();
                    }
                }

                // TODO regiter person
                if (thirdCompleteDTO.getPersonDTO() != null) {
                    Either<IException, Long> commonThirdEither = personBusiness.createPerson(thirdCompleteDTO.getPersonDTO());
                    if (commonThirdEither.isRight()){
                        id_person=commonThirdEither.right().value();
                    }
                }
                    // TODO regiter third

                ThirdDTO thirdDTO= new ThirdDTO(thirdCompleteDTO.getId_third_type(),id_common_third,id_common_basicinfo,id_person,thirdCompleteDTO.getId_third_father());
                thirdDAO.create(thirdDTO);
                id_third=thirdDAO.getPkLast();

                    // TODO after regiter directory
                if (thirdCompleteDTO.getDirectoryDTO()!=null){
                    Either<IException, Long> directoryEither = directoryBusiness.createDirectory(thirdCompleteDTO.getDirectoryDTO());
                    if (directoryEither.isRight()){
                        id_directory=directoryEither.right().value();

                        if (id_directory>0){
                            Either<IException, Long> locationEither = locationBusiness.createLocation(new LocationDTO(id_third, id_directory, new Common_ThirdDTO(new Long(1), new Date(), new Date())));

                            if (locationEither.isRight()){

                            }
                        }
                    }
                }



                return Either.right(Long.valueOf(thirdDAO.getPkLast()));
            } else {
                msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param id_third
     * @param thirdDTO
     * @return
     */
    public Either<IException, Long> updateThird(Long id_third ,ThirdCompleteDTO thirdDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update  Third ||||||||||||||||| ");
            if (thirdDTO != null) {

                Third actualThird = thirdDAO.read(id_third, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null).get(0);

                // Validate Id_third_father inserted, if null, set the Id_third_father to the actual in the database, if not it is formatted to sql
                if (thirdDTO.getId_third_father() == null || thirdDTO.getId_third_father().equals("")) {
                    thirdDTO.setId_third_father(actualThird.getId_third_father());
                } else {
                    thirdDTO.setId_third_father(formatoLongSql(thirdDTO.getId_third_father()));
                }

                // Validate Id_third_type inserted, if null, set the Id_third_type to the actual in the database, if not it is formatted to sql
                if (thirdDTO.getId_third_type() == null || thirdDTO.getId_third_type().equals("")) {
                    thirdDTO.setId_third_type(actualThird.getType().getId_third_type());
                } else {
                    thirdDTO.setId_third_type(formatoLongSql(thirdDTO.getId_third_type()));
                }
                
                // Update common third
                commonThirdBusiness.updateCommonThird(actualThird.getState().getId_common_third(), thirdDTO.getCommon_thirdDTO());
                
                // Update common basic info
                commonBasicInfoBusiness.updateCommonBasicInfo(actualThird.getInfo().getId_common_basicinfo(), thirdDTO.getBasicInfoDTO());
                
                // Update directory
                directoryBusiness.updateDirectory(((Directory) actualThird.getDirectory()).getId_directory(), thirdDTO.getDirectoryDTO());

                thirdDAO.update(id_third, thirdDTO);

                return Either.right(null);

            } else {
                msn.add("It does not recognize  Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idThird
     * @return
     */
    public Either<IException, Long> deleteThird(Long idThird) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idThird != null && idThird > 0) {

                Long idCommon = thirdDAO.delete(formatoLongSql(idThird)).get(0);
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idThird);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
