package com.name.business.businesses;

import com.name.business.DAOs.PersonDAO;
import com.name.business.entities.Directory;
import com.name.business.entities.Employee;
import com.name.business.entities.Person;
import com.name.business.entities.UserThird;
import com.name.business.representations.PersonDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class PersonBusiness {
    private PersonDAO personDAO;
    private CommonBasicInfoBusiness commonBasicInfoBusiness;
    private CommonThirdBusiness commonThirdBusiness;
    private DirectoryBusiness directoryBusiness;
    private EmployeeBusiness employeeBusiness;
    private UserThirdBusiness userThirdBusiness;

    /**
     * 
     * @param personDAO
     * @param commonBasicInfoBusiness
     * @param commonThirdBusiness
     * @param directoryBusiness
     * @param employeeBusiness
     * @param userThirdBusiness
     */
    public PersonBusiness(PersonDAO personDAO, CommonBasicInfoBusiness commonBasicInfoBusiness, CommonThirdBusiness commonThirdBusiness, DirectoryBusiness directoryBusiness, EmployeeBusiness employeeBusiness, UserThirdBusiness userThirdBusiness) {
        this.personDAO = personDAO;
        this.commonBasicInfoBusiness = commonBasicInfoBusiness;
        this.commonThirdBusiness = commonThirdBusiness;
        this.directoryBusiness = directoryBusiness;
        this.employeeBusiness = employeeBusiness;
        this.userThirdBusiness = userThirdBusiness;
    }

    /**
     * 
     * @param personDTO
     * @return
     */
    public Either<IException, Long> createPerson(PersonDTO personDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common_basicinfo = null;
        Long id_common_third = null;
        Long id_directory = null;
        Long id_person = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Person ||||||||||||||||| ");
            if (personDTO != null) {
                if (personDTO.getCommonBasicInfoDTO() != null) {
                    Either<IException, Long> commonBasicInfoEither = commonBasicInfoBusiness.createCommonBasicInfo(personDTO.getCommonBasicInfoDTO());
                    if (commonBasicInfoEither.isRight()) {
                        id_common_basicinfo = commonBasicInfoEither.right().value();
                    }
                }

                if (personDTO.getCommon_thirdDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(personDTO.getCommon_thirdDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_third = commonThirdEither.right().value();
                    }
                }
                
                personDAO.create(personDTO, id_directory, id_common_third, id_common_basicinfo);

                id_person = personDAO.getPkLast();

                // TODO regiter directory
                if (personDTO.getDirectoryDTO() != null) {
                    directoryBusiness.createDirectory(personDTO.getDirectoryDTO());
                }

                if (personDTO.getEmployee() != null) {
                    employeeBusiness.createEmployee(id_person, personDTO.getEmployee());
                }

                if (personDTO.getUuid() != null) {
                    userThirdBusiness.createUserThird(id_person, personDTO.getUuid());
                }

                return Either.right(Long.valueOf(personDAO.getPkLast()));

            } else {
                msn.add("It does not recognize Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * 
     * @param id_person
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    public Either<IException, List<Person>> getPerson(Long id_person, String first_name,
                                                      String second_name, String first_lastname,
                                                      String second_lastname, Date birthday,
                                                      Long id_dir_person, Long id_cbi_person,
                                                      Long id_doctype_person, String doc_person,
                                                      Long typedoc_person, String fullname_person,
                                                      String img_person,
                                                      Long id_c_th_person, Integer state_person,
                                                      Date creation_person, Date modify_person) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults data Person ||||||||||||||||| ");
            List<Person> read = personDAO.read(
                    formatoLongSql(id_person),
                    formatoLIKESql(first_name),
                    formatoLIKESql(second_name),
                    formatoLIKESql(first_lastname),
                    formatoLIKESql(second_lastname),
                    formatoDateSql(birthday),
                    formatoLongSql(id_dir_person),
                    formatoLongSql(id_cbi_person),
                    formatoLongSql(id_doctype_person),
                    formatoStringSql(doc_person),
                    formatoLongSql(typedoc_person),
                    formatoLIKESql(fullname_person),
                    formatoStringSql(img_person),
                    formatoLongSql(id_c_th_person),
                    formatoIntegerSql(state_person),
                    formatoDateSql(creation_person),
                    formatoDateSql(modify_person));
            System.out.println("Read realized correctly");

            // TODO load directory with list special

            for (int i = 0; i < read.size(); i++) {
                Long id_directory = (Long) read.get(0).getDirectory();
                if (id_directory > 0) {

                    Either<IException, List<Directory>> directoryEither = directoryBusiness.getDirectory(id_directory, null, null, null, null, null, null, null, null);
                    if (directoryEither.isRight()) {
                        List<Directory> directoryList = directoryEither.right().value();
                        if (directoryList.size() > 0) {
                            read.get(i).setDirectory(directoryList.get(0));

                        }
                    }


                }

                Either<IException, List<Employee>> employeeEither = employeeBusiness.getEmployee(null, read.get(i).getId_person(), null, null, null, null, null);
                if (employeeEither.isRight()) {
                    List<Employee> employees = employeeEither.right().value();
                    if (employees.size() > 0) {
                        read.get(i).setEmployee(employees);
                    }

                }

                Either<IException, List<UserThird>> userThirdEither = userThirdBusiness.getUserThird(null, null, read.get(i).getId_person(), null, null, null, null);
                if (userThirdEither.isRight()) {
                    List<UserThird> user = userThirdEither.right().value();
                    if (user.size() > 0) {
                        read.get(i).setUUID(user);
                    }

                }


            }

            return Either.right(read);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param idPerson
     * @param personDTO
     * @return
     */
    public Either<IException, Long> updatePerson(Long idPerson, PersonDTO personDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Common Third ||||||||||||||||| ");
            if (idPerson != null && idPerson > 0) {

                Long idCommon = personDAO.delete(formatoLongSql(idPerson)).get(0);
                Long idCommonBasicInfo = personDAO.deleteCommonBasicInfo(formatoLongSql(idPerson)).get(0);
                Long idDirectory = personDAO.deleteDirectory(formatoLongSql(idPerson)).get(0);

                Person actualPerson = personDAO.read(idPerson, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null).get(0);

                // Validate First_name inserted, if null, set the First_name to the actual in the database, if not it is formatted to sql
                if (personDTO.getFirst_name() == null || personDTO.getFirst_name().equals("")) {
                    personDTO.setFirst_name(actualPerson.getFirst_name());
                } else {
                    personDTO.setFirst_name(formatoStringSql(personDTO.getFirst_name()));
                }

                // Validate Second_name inserted, if null, set the Second_name to the actual in the database, if not it is formatted to sql
                if (personDTO.getSecond_name() == null) {
                    personDTO.setSecond_name(actualPerson.getSecond_name());
                } else {
                    personDTO.setSecond_name(formatoStringSql(personDTO.getSecond_name()));
                }

                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (personDTO.getFirst_lastname() == null || personDTO.getFirst_lastname().equals("")) {
                    personDTO.setFirst_lastname(actualPerson.getFirst_lastname());
                } else {
                    personDTO.setFirst_lastname(formatoStringSql(personDTO.getFirst_lastname()));
                }

                // Validate Second_lastname inserted, if null, set the Second_lastname to the actual in the database, if not it is formatted to sql
                if (personDTO.getSecond_lastname() == null || personDTO.getSecond_lastname().equals("")) {
                    personDTO.setSecond_lastname(actualPerson.getSecond_lastname());
                } else {
                    personDTO.setSecond_lastname(formatoStringSql(personDTO.getSecond_lastname()));
                }

                // Validate Birthday inserted, if null, set the Birthday to the actual in the database, if not it is formatted to sql
                if (personDTO.getBirthday() == null) {
                    personDTO.setBirthday(actualPerson.getBirthday());
                } else {
                    personDTO.setBirthday(formatoDateSql(personDTO.getBirthday()));
                }

                // Common basic info update
                commonBasicInfoBusiness.updateCommonBasicInfo(idCommonBasicInfo, personDTO.getCommonBasicInfoDTO());

                // common Thid business update
                commonThirdBusiness.updateCommonThird(idCommon, personDTO.getCommon_thirdDTO());

                // Directory update
                directoryBusiness.updateDirectory(idDirectory, personDTO.getDirectoryDTO());

                // Person update
                personDAO.update(idPerson,personDTO);

                return Either.right(null);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idPerson

     * @return
     */
    public Either<IException, Long> deletePerson(Long idPerson) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete person ||||||||||||||||| ");
            if (idPerson != null && idPerson > 0) {


                Long idCommon = personDAO.delete(formatoLongSql(idPerson)).get(0);
                Long idCommonBasicInfo = personDAO.deleteCommonBasicInfo(formatoLongSql(idPerson)).get(0);
                Long idDirectory = personDAO.deleteDirectory(formatoLongSql(idPerson)).get(0);
                commonThirdBusiness.deleteCommonThird(idCommon);
                directoryBusiness.deleteDirectory(idDirectory);
                //TO DO when it would be determine how to delete basic info
                commonBasicInfoBusiness.deleteCommonBasicInfo(idCommonBasicInfo);
                return Either.right(idPerson);
            } else {
                msn.add("It does not recognize ID person, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
