package com.name.business.businesses;

import com.name.business.DAOs.UserThirdDAO;
import com.name.business.entities.UserThird;
import com.name.business.representations.UserThirdDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoDateSql;

/**
 * Created by trossky on 14/07/17.
 */
public class UserThirdBusiness {

   private UserThirdDAO userThirdDAO;
   private CommonThirdBusiness commonThirdBusiness;

    /**
     *
     * @param userThirdDAO
     * @param commonThirdBusiness
     */
    public UserThirdBusiness(UserThirdDAO userThirdDAO, CommonThirdBusiness commonThirdBusiness) {
        this.userThirdDAO = userThirdDAO;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    /**
     *
     * @param id_user_third
     * @param UUID
     * @param id_person
     * @param id_common_third
     * @param state
     * @param creation_user
     * @param modify_user
     * @return
     */
    public Either<IException, List<UserThird>> getUserThird(Long id_user_third, String UUID, Long id_person,
                                                            Long id_common_third, Integer state,
                                                            Date creation_user, Date modify_user) {
        List<String> msn = new ArrayList<>();

        try{
            System.out.println("|||||||||||| Starting consults data User ||||||||||||||||| ");

            List<UserThird> read = userThirdDAO.read(formatoLongSql(id_user_third),formatoStringSql(UUID),
                                                    formatoLongSql(id_person),formatoLongSql(id_common_third),
                    formatoIntegerSql(state),formatoDateSql(creation_user),formatoDateSql(modify_user));

            return Either.right(read);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    /**
     *
     * @param id_person
     * @param userThirdDTO
     * @return
     */
    public Either<IException, Long> createUserThird(Long id_person,UserThirdDTO userThirdDTO) {
        List<String> msn = new ArrayList<>();

        Long id_common_third=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation User Third ||||||||||||||||| ");
            if (userThirdDTO != null) {

                if (userThirdDTO.getState()!=null){
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(userThirdDTO.getState());

                    if (commonThirdEither.isRight()){
                        id_common_third=commonThirdEither.right().value();
                    }
                }

                userThirdDAO.create(userThirdDTO.getUUID(),formatoLongSql(id_person),id_common_third);

                return Either.right(userThirdDAO.getPkLast());

            } else {
                msn.add("It does not recognize User Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> updateUserThird(Long idUser, UserThirdDTO userThirdDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE DocumentType ||||||||||||||||| ");
            if (idUser != null && idUser > 0) {

                UserThird actualUserThird = userThirdDAO.read(formatoLongSql(idUser), null, null, null, null, null, null).get(0);

                // Validate UUID inserted, if null, set the UUID to the actual in the database, if not it is formatted to sql
                if (userThirdDTO.getUUID() == null || userThirdDTO.getUUID().equals("")) {
                    userThirdDTO.setUUID(actualUserThird.getUUID());
                } else {
                    userThirdDTO.setUUID(formatoStringSql(userThirdDTO.getUUID()));
                }

                userThirdDAO.update(formatoLongSql(idUser), userThirdDTO);
                Long idCommon = userThirdDAO.delete(formatoLongSql(idUser)).get(0);
                commonThirdBusiness.updateCommonThird(idCommon, userThirdDTO.getState());
                return Either.right(idUser);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
    
    /**
     *
     * @param idUser
     * @return
     */
    public Either<IException, Long> deleteUserThird(Long idUser) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idUser != null && idUser > 0) {

                Long idCommon = userThirdDAO.delete(formatoLongSql(idUser)).get(0);

                // TO DO when thidLocation has common third
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idUser);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
