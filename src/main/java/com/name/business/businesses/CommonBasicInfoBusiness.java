package com.name.business.businesses;


import com.name.business.DAOs.CommonBasicInfoDAO;
import com.name.business.entities.CommonBasicInfo;
import com.name.business.representations.CommonBasicInfoDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class CommonBasicInfoBusiness {
    private CommonBasicInfoDAO commonBasicInfoDAO;

    /**
     *
     * @param commonBasicInfoDAO
     */
    public CommonBasicInfoBusiness(CommonBasicInfoDAO commonBasicInfoDAO) {
        this.commonBasicInfoDAO = commonBasicInfoDAO;
    }

    /**
     *
     * @param id_common_third
     * @param fullname
     * @param id_documen_type
     * @param document_number
     * @param type_document
     * @param img
     * @return
     */
    public Either<IException, List<CommonBasicInfo>> getCommonBasicInfo(Long id_common_third,String fullname,
    Long id_documen_type,String document_number,String type_document,
    String img) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Common Basic Info ||||||||||||||||| ");
            if (null == null) {
                List<CommonBasicInfo> read = commonBasicInfoDAO.read(formatoLongSql(id_common_third),formatoLIKESql(fullname),
                        formatoLongSql(id_documen_type),formatoLIKESql(document_number),formatoLIKESql(type_document),
                        formatoStringSql(img));
                return Either.right(read);

            } else {
                msn.add("It does not recognize Common Basic Info, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param basicInfoDTO
     * @return
     */
    public Either<IException, Long> createCommonBasicInfo(CommonBasicInfoDTO basicInfoDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Common Basic Info ||||||||||||||||| ");
            if (basicInfoDTO != null) {
                commonBasicInfoDAO.create(basicInfoDTO);

                return Either.right(Long.valueOf(commonBasicInfoDAO.getPkLast()));

            } else {
                msn.add("It does not recognize Common Basic Info, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param id_common_basic_info
     * @param basicInfoDTO
     * @return
     */
    public Either<IException, Long> updateCommonBasicInfo(Long id_common_basic_info, CommonBasicInfoDTO basicInfoDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Common Basic Info ||||||||||||||||| ");
            if (basicInfoDTO != null) {

                return Either.right(Long.valueOf(commonBasicInfoDAO.update(formatoLongSql(id_common_basic_info),basicInfoDTO)));

            } else {
                msn.add("It does not recognize Common Basic Info, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idCommonBasicInfo
     * @return
     */
    public Either<IException, Long> deleteCommonBasicInfo(Long idCommonBasicInfo) {
        return null;
    }
}
