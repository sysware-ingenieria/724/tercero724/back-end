package com.name.business.businesses;

import com.name.business.DAOs.EmployeeDAO;
import com.name.business.entities.Employee;
import com.name.business.representations.EmployeeDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoDateSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoDoubleSql;

/**
 * Created by trossky on 14/07/17.
 */
public class EmployeeBusiness {

    private EmployeeDAO employeeDAO;
    private CommonThirdBusiness commonThirdBusiness;

    /**
     * @param employeeDAO
     */
    public EmployeeBusiness(EmployeeDAO employeeDAO, CommonThirdBusiness commonThirdBusiness) {
        this.employeeDAO = employeeDAO;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    public Either<IException, List<Employee>> getEmployee(Long id_employee, Long id_person, Double salary,
                                                          Long id_common_third, Integer state,
                                                          Date creation_employee, Date modify_employee) {
        List<String> msn = new ArrayList<>();
        try {
            System.out.println("|||||||||||| Starting consults data Employee ||||||||||||||||| ");

            List<Employee> read = employeeDAO.read(formatoLongSql(id_employee), formatoLongSql(id_person), formatoDoubleSql(salary), formatoLongSql(id_common_third),
                    formatoIntegerSql(state), formatoDateSql(creation_employee), formatoDateSql(modify_employee));

            return Either.right(read);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }


    public Either<IException, Long> createEmployee(Long id_person, EmployeeDTO employeeDTO) {
        List<String> msn = new ArrayList<>();

        Long id_common_third=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Employee ||||||||||||||||| ");
            if(id_person == null || id_person <= 0){
                msn.add("The employee needs an id person to be created");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            // TO DO else validate the existence of id Person

            if (employeeDTO != null) {

                if (employeeDTO.getState()!=null){
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(employeeDTO.getState());

                    if (commonThirdEither.isRight()){
                        id_common_third=commonThirdEither.right().value();
                    }
                }

                employeeDAO.create(employeeDTO,id_person,id_common_third);

                return Either.right(Long.valueOf(employeeDAO.getPkLast()));

            } else {
                msn.add("It does not recognize Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * Update the employee based of the params.
     *
     * @param id_Employee
     * @param empDTO
     * @return
     */
    public Either<IException, Long> updateEmployee(Long id_Employee,
                                                   EmployeeDTO empDTO
    ) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE EMPLOYEE ||||||||||||||||| ");
            if (id_Employee != null && id_Employee > 0) {

                List<Employee> employees = employeeDAO.read(formatoLongSql(id_Employee), null, null, null, null, null, null);
                Employee actualEmployee = null;
                if(employees.size()>0){
                    actualEmployee = employees.get(0);
                    /**
                     * Analyze the salary if is null or less than 0, then it find in the database the actual data
                     * if not, format the result to prevent SQL injections
                     */
                    if (empDTO.getSalary() == null && empDTO.getSalary() <= 0) {
                        empDTO.setSalary(actualEmployee.getSalary());
                    } else {
                        empDTO.setSalary(formatoDoubleSql(empDTO.getSalary()));
                    }

                    employeeDAO.update(formatoLongSql(id_Employee), empDTO);
                    // If the update was done correctly, update de common third information
                    Long idCommon = employeeDAO.read(formatoLongSql(id_Employee), null, null, null, null, null, null).get(0).getCommonThird().getId_common_third();
                    commonThirdBusiness.updateCommonThird(idCommon, empDTO.getState());
                    return Either.right(id_Employee);
                }
                else{
                    msn.add("The ID employee does not exist.");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

            } else {
                msn.add("It does not recognize ID Employee, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * Change the state of employee to 0
     *
     * @param id_Employee
     * @return
     */
    public Either<IException, Long> deleteEmployee(Long id_Employee) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE ( CHANGE STATE) EMPLOYEE ||||||||||||||||| ");
            // Verify that the employee has correct values
            if (id_Employee != null && id_Employee > 0) {
                Long idCommon = employeeDAO.delete(id_Employee).get(0);
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(id_Employee);
            } else {
                msn.add("It does not recognize ID Employee, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}


