package com.name.business.businesses;


import com.name.business.DAOs.CommonThirdDAO;
import com.name.business.entities.CommonThird;
import com.name.business.representations.Common_ThirdDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class CommonThirdBusiness {
    private CommonThirdDAO commonThirdDAO;

    /**
     * @param commonThirdDAO
     */
    public CommonThirdBusiness(CommonThirdDAO commonThirdDAO) {
        this.commonThirdDAO = commonThirdDAO;
    }

    /**
     * @param id_common_third
     * @param state
     * @param creation_date
     * @param modify_date
     * @return
     */
    public Either<IException, List<CommonThird>> getCommonThird(Long id_common_third, Integer state, Date creation_date, Date modify_date) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Common Type ||||||||||||||||| ");
            if (null == null) {
                List<CommonThird> read = commonThirdDAO.read(formatoLongSql(id_common_third), formatoIntegerSql(state), formatoDateSql(creation_date), formatoDateSql(modify_date));
                return Either.right(read);

            } else {
                msn.add("It does not recognize Common Type, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param commonThirdDTO
     * @return
     */
    public Either<IException, Long> createCommonThird(Common_ThirdDTO commonThirdDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Common Third ||||||||||||||||| ");
            if (commonThirdDTO != null) {

                //It validate the modify date, if null, set the creation date to the actual date
                if (commonThirdDTO.getCreation_date() == null) {
                    commonThirdDTO.setCreation_date(new Date());
                }

                //It validate the modify date, if null, set the modify date to the actual date
                if (commonThirdDTO.getModify_date() == null) {
                    commonThirdDTO.setModify_date(new Date());
                }

                //It validate the modify date, if null, set the state to 1
                if (commonThirdDTO.getState() == null) {
                    commonThirdDTO.setState(Long.valueOf(1));
                }

                commonThirdDAO.create(commonThirdDTO);
                return Either.right(Long.valueOf(commonThirdDAO.getPkLast()));

            } else {
                msn.add("It does not recognize Common Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_common_third
     * @param cdDTO
     * @return
     */
    public Either<IException, Long> updateCommonThird(Long id_common_third, Common_ThirdDTO cdDTO) {
        List<String> msn = new ArrayList<>();
        Either<IException, Long> answer;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE Common Third ||||||||||||||||| ");
            if (id_common_third != null && id_common_third != 0) {

                if (cdDTO.getModify_date() == null) {
                    cdDTO.setModify_date(new Date());
                }

                if (cdDTO.getCreation_date() == null) {
                    Date createDate = commonThirdDAO.read(formatoLongSql(id_common_third), null, null, null).get(0).getCreate_date();
                    cdDTO.setCreation_date(createDate);
                }
                commonThirdDAO.update(formatoLongSql(id_common_third), cdDTO);
                return Either.right(id_common_third);
            } else {
                msn.add("It does not recognize Common Third, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param idCommon_Third
     * @return
     */
    public Either<IException, Long> deleteCommonThird(Long idCommon_Third) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE Common Third ||||||||||||||||| ");
            commonThirdDAO.delete(formatoLongSql(idCommon_Third), new Date());
            if (idCommon_Third != 0) {
                return Either.right(Long.valueOf(commonThirdDAO.getPkLast()));
            } else {
                msn.add("It does not recognize ID Common Third, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_common_third
     * @param state
     * @param creation_date
     * @param modify_date
     * @return
     */
    public Either<IException, Long> permanentDeleteCommonThird(Long id_common_third, Integer state, Date creation_date, Date modify_date) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting PERMANENT DELETE Common Third ||||||||||||||||| ");
            if (null == null) {
                //commonThirdDAO.permanentDelete(formatoLongSql(id_common_third),formatoIntegerSql(state),
                // formatoDateSql(creation_date),formatoDateSql(modify_date));
                return null;

            } else {
                msn.add("It does not recognize id_Common Third, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
