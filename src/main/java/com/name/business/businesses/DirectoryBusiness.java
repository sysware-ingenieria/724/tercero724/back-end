package com.name.business.businesses;

import com.name.business.DAOs.DirectoryDAO;
import com.name.business.entities.Directory;
import com.name.business.entities.Mail;
import com.name.business.entities.Phone;
import com.name.business.representations.DirectoryDTO;
import com.name.business.representations.MailDTO;
import com.name.business.representations.PhoneDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by trossky on 14/07/17.
 */
public class DirectoryBusiness {
    
    private DirectoryDAO directoryDAO;
    private CommonThirdBusiness commonThirdBusiness;
    private MailBusiness mailBusiness;
    private PhoneBusiness phoneBusiness;

    /***
     * 
     * @param directoryDAO
     * @param commonThirdBusiness
     * @param mailBusiness
     * @param phoneBusiness
     */
    public DirectoryBusiness(DirectoryDAO directoryDAO, CommonThirdBusiness commonThirdBusiness, MailBusiness mailBusiness, PhoneBusiness phoneBusiness) {
        this.directoryDAO = directoryDAO;
        this.commonThirdBusiness = commonThirdBusiness;
        this.mailBusiness = mailBusiness;
        this.phoneBusiness = phoneBusiness;
    }

    /**
     * 
     * @param directoryDTO
     * @return
     */
    public Either<IException, Long> createDirectory(DirectoryDTO directoryDTO) {
        List<String> msn = new ArrayList<>();
        Long id_directory = null;
        Long id_common_third = null;
        System.out.println(directoryDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Third  ||||||||||||||||| ");
            if (directoryDTO != null) {
                if (directoryDTO.getCommon_thirdDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(directoryDTO.getCommon_thirdDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_third = commonThirdEither.right().value();
                    }
                }

                directoryDAO.create(id_common_third, directoryDTO.getAddress(), directoryDTO.getCountry(), directoryDTO.getCity(), directoryDTO.getWebpage());
                id_directory = directoryDAO.getPkLast();
                if (id_directory > 0) {
                    // TODO  register mail
                    List<MailDTO> mailDTOList= new ArrayList<>();
                    if (directoryDTO.getMailDTOList()!=null)
                        mailDTOList = directoryDTO.getMailDTOList();
                    mailBusiness.createMail(id_directory, mailDTOList);
                    // TODO  register telephone
                    List<PhoneDTO> phoneDTOList= new ArrayList<>();
                    if (directoryDTO.getPhoneDTOList()!=null)
                         phoneDTOList= directoryDTO.getPhoneDTOList();
                    phoneBusiness.createPhone(id_directory, phoneDTOList);
                }
                return Either.right(Long.valueOf(directoryDAO.getPkLast()));
            } else {
                msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * 
     * @param id_directory
     * @param address
     * @param country
     * @param city
     * @param webpage
     * @param id_ct_directory
     * @param state_dir
     * @param creation_dir
     * @param modify
     * @return
     */
    public Either<IException, List<Directory>> getDirectory(Long id_directory, String address,
                                                            String country, String city,
                                                            String webpage, Long id_ct_directory,
                                                            Integer state_dir, Date creation_dir,
                                                            Date modify) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults data Directory ||||||||||||||||| ");
            List<Directory> read = directoryDAO.read(formatoLongSql(id_directory), formatoLIKESql(address), formatoLIKESql(country),
                    formatoLIKESql(city), formatoLIKESql(webpage), formatoLongSql(id_ct_directory), formatoIntegerSql(state_dir),
                    formatoDateSql(creation_dir), formatoDateSql(modify));


            for (int i = 0; i < read.size(); i++) {
                Long id_dir = read.get(i).getId_directory();

                // TODO load mails of directory
                Either<IException, List<Mail>> mails = mailBusiness.getMails(null, null, null, id_dir,null,null,null,null);
                if (mails.isRight()) {
                    List<Mail> mailList = mails.right().value();
                    if (mailList.size() > 0) {
                        read.get(i).setMails(mailList);
                    }

                }

                // TODO load phones of directory
                Either<IException, List<Phone>> phoneListEither = phoneBusiness.getPhones(null, null, null, id_dir,null,null,null,null);
                if (phoneListEither.isRight()) {
                    List<Phone> phoneList = phoneListEither.right().value();
                    if (phoneList.size() > 0) {
                        read.get(i).setPhones(phoneList);
                    }

                }


            }

            return Either.right(read);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        } 
    }
    
    public Either<IException, Long> updateDirectory(Long idDirectory, DirectoryDTO directoryDTO){
        List<String> msn = new ArrayList<>();
        try{
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE Directory ||||||||||||||||| ");
            if(idDirectory != null && idDirectory > 0){

                List<Directory> directories = directoryDAO.read(idDirectory, null, null, null,  null, null, null, null, null);
                Directory actualDirectory = null;

                if(directories.size()>0) {
                    actualDirectory = directoryDAO.read(idDirectory, null, null, null, null, null, null, null, null).get(0);

                    // Validate address inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                    if (directoryDTO.getAddress() == null || directoryDTO.getAddress().equals("")) {
                        directoryDTO.setAddress(actualDirectory.getAddress());
                    } else {
                        directoryDTO.setAddress(formatoStringSql(directoryDTO.getAddress()));
                    }

                    // Validate city inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                    if (directoryDTO.getCity() == null || directoryDTO.getCity().equals("")) {
                        directoryDTO.setAddress(actualDirectory.getCity());
                    } else {
                        directoryDTO.setAddress(formatoStringSql(directoryDTO.getCity()));
                    }

                    // Validate country inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                    if (directoryDTO.getCountry() == null || directoryDTO.getCountry().equals("")) {
                        directoryDTO.setAddress(actualDirectory.getCountry());
                    } else {
                        directoryDTO.setAddress(formatoStringSql(directoryDTO.getCountry()));
                    }

                    // Validate Webpage inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                    if (directoryDTO.getWebpage() == null || directoryDTO.getWebpage().equals("")) {
                        directoryDTO.setAddress(actualDirectory.getWebpage());
                    } else {
                        directoryDTO.setAddress(formatoStringSql(directoryDTO.getWebpage()));
                    }

                    // Update the directory in the database
                    directoryDAO.update(formatoLongSql(idDirectory), directoryDTO);

                    // Update the common third information for the Updated directory
                    Long idCommon = directoryDAO.delete(formatoLongSql(idDirectory)).get(0);
                    commonThirdBusiness.updateCommonThird(idCommon, directoryDTO.getCommon_thirdDTO());

                    return Either.right(idDirectory);
                }
                else{
                    msn.add("It does not exist a directory with that ID");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }else {
                msn.add("It does not recognize ID directory, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> deleteDirectory(Long idDirectory){
        List<String> msn = new ArrayList<>();
        try{
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if(idDirectory != null && idDirectory > 0){

                Long idCommon = directoryDAO.delete(formatoLongSql(idDirectory)).get(0);
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idDirectory);
            }else {
                msn.add("It does not recognize ID directory, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
    
}
