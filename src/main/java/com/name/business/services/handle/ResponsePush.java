package com.name.business.services.handle;

/**
 * Created by luis on 30/03/17.
 */
public class ResponsePush {
    private String msn_error;
    private String value_exitoso;
    public ResponsePush(String msn_error, String value_exitoso) {
        this.msn_error = msn_error;
        this.value_exitoso = value_exitoso;
    }
    public String getMsn_error() {
        return msn_error;
    }
    public void setMsn_error(String msn_error) {
        this.msn_error = msn_error;
    }
    public String getValue_exitoso() {
        return value_exitoso;
    }
    public void setValue_exitoso(String value_exitoso) {
        this.value_exitoso = value_exitoso;
    }
}
