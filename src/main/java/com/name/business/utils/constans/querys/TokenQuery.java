package com.name.business.utils.constans.querys;

/**
 * Created by luis_ on 29/01/2017.
 */
public class TokenQuery {
    public static final String TABLE=" tokens ";
    public static final String INSERT_TOKEN="INSERT INTO "+TABLE+" (key_token,create_time) VALUES ";

    public static final String SELECT_COLUMNS=" tk.id_token AS id_token ,tk.key_token as key_token,us.id_role as id_role ,us.id_person as id_person,us.id as id_user, us.username as username,pe.full_name as full_name, pe.last_name as last_name ";
    public static final String JOINS_SESSION_USERS_PERSONS="RIGHT JOIN sessions AS se ON tk.id_token=se.id_token RIGHT JOIN users AS us ON se.id_user=us.id RIGHT JOIN persons as pe ON us.id_person = pe.id ";

    public static final String SELECT_TOKEN="SELECT "+SELECT_COLUMNS+"FROM"+TABLE+" as tk "+JOINS_SESSION_USERS_PERSONS+" WHERE tk.key_token= ";

    public static final String SELECT_URIS_BY_ROLE=" SELECT mc.uri FROM roles_permissions rp INNER JOIN microservices mc ON rp.id_microservices = mc.id ";

    public static final String DELETE_TOKEN_BY_KEY="DELETE FROM"+TABLE+" WHERE key_token= ";
}
