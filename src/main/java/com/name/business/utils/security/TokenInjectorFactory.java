package com.name.business.utils.security;



import com.name.business.entities.Aplicacion;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.glassfish.hk2.api.Factory;


import static com.name.business.utils.constans.K.TOKEN_ATTRIBUTE;


public class TokenInjectorFactory implements Factory<Aplicacion> {

    private HttpServletRequest httpRequest;

    @Inject
    public TokenInjectorFactory(HttpServletRequest httpRequest) {



        this.httpRequest = httpRequest;
    }

    @Override
    public Aplicacion provide() {
        Aplicacion aplicacion = (Aplicacion) httpRequest.getAttribute(AuthorizationUri.TOKEN_ATTRIBUTE);

        return aplicacion;
    }

    @Override
    public void dispose(Aplicacion aplicacion) {

    }
}
