package com.name.business.utils.security;


import com.name.business.businesses.TokenBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.representations.TiketDTO;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.util.ArrayList;

import static com.name.business.utils.constans.K.URI_BASE;


public class AuthorizationUri implements Filter {
    public static final String TOKEN_ATTRIBUTE = "Token";

    private final ArrayList<String> publicURIs;
    private final TokenBusiness tokenBusiness;

    public AuthorizationUri(TokenBusiness tokenBusiness) {
        this.tokenBusiness = tokenBusiness;
        this.publicURIs = new ArrayList<>();
        publicURIs.add(URI_BASE+"/colleges/names");
        publicURIs.add(URI_BASE+"/auth");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest=(HttpServletRequest) servletRequest;

        String TOKEN_VALUE=httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
        System.out.println("\n\n\n\n\n\n\n\n   ---");
        System.out.println(TOKEN_VALUE);
        System.out.println("\n\n\n\n\n\n\n\n");
        String HTTP_VERB= httpRequest.getMethod();
        String REQUESTED_URI=httpRequest.getRequestURI();

        /**
         * Typycally thr browser sends an OPTIONS request by itself. Due to the Browser does not include the
         * Authorization header we will allow all OPTIONS request to pass
         * */
        if (HTTP_VERB.equalsIgnoreCase("OPTIONS")){

            //Go to next filter. If this filter is the last one, then go the resource.
            filterChain.doFilter(servletRequest,servletResponse);

        }else{

            boolean isPublic= false;

            for (int i = 0; i < publicURIs.size() && !isPublic; i++) {
                isPublic=UrlSanitize.urlsMatch(publicURIs.get(i),REQUESTED_URI);
            }
            if (isPublic){
                //Go to next filter. If this filter is the last one, then go the resource.
                filterChain.doFilter(servletRequest,servletResponse);
            }else{

                TiketDTO tiketDTO= new TiketDTO(TOKEN_VALUE,HTTP_VERB,REQUESTED_URI);
                Either<IException,Aplicacion> tokenIsAuthorizedEither=tokenBusiness.tokenIsAuthorized(tiketDTO);

                if (tokenIsAuthorizedEither.isRight()){

                    Aplicacion aplicacion=tokenIsAuthorizedEither.right().value();

                    if (aplicacion!=null){
                        // puts the token in the http request.
                        servletRequest.setAttribute(TOKEN_ATTRIBUTE,aplicacion);

                        // Go the next filter, If this filter is the last one, then go to resource.
                        filterChain.doFilter(servletRequest,servletResponse);

                    }else{
                        // Response with the given status code (401)
                        HttpServletResponse  httpResponse=(HttpServletResponse) servletResponse;
                        httpResponse.setContentType("application/json");
                        httpResponse.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
                    }

                }else{
                    // Response with the given status code (401)
                    HttpServletResponse  httpResponse=(HttpServletResponse) servletResponse;
                    httpResponse.setContentType("application/json");
                    httpResponse.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());

                    // Sends the exception in the body
                    IException exceptionGenerated= tokenIsAuthorizedEither.left().value();
                    httpResponse.getWriter().write(exceptionGenerated.getMessage());
                }

            }



        }
    }

    @Override
    public void destroy() {

    }
}
