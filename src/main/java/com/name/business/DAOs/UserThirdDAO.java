package com.name.business.DAOs;

import com.name.business.entities.UserThird;
import com.name.business.mappers.UserThirdMapper;
import com.name.business.representations.UserThirdDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(UserThirdMapper.class)
public interface UserThirdDAO {

    /**
     *
     * @param uuid
     * @param id_person
     * @param id_common_third
     * @return
     */
    @SqlUpdate("INSERT INTO USER_THIRD " +
            "(UUID, ID_COMMON_THIRD, ID_PERSON) VALUES" +
            "(:UUID, :id_common_third,:id_person )")
    int create(@Bind("UUID") String uuid,@Bind("id_person")Long id_person,@Bind("id_common_third") Long id_common_third);

    /**
     *
     * @param id_user_third
     * @param UUID
     * @param id_person
     * @param id_common_third
     * @param state
     * @param creation_user
     * @param modify_user
     * @return
     */
    @SqlQuery("SELECT * FROM V_USER V_US " +
            "  WHERE (V_US.ID_USER_THIRD =:ID_USER_THIRD OR :ID_USER_THIRD IS NULL ) AND" +
            "        (V_US.UUID =:UUID OR :UUID IS NULL ) AND" +
            "        (V_US.ID_PERSON =:ID_PERSON OR :ID_PERSON IS NULL ) AND" +
            "        ( V_US.ID_COMMON_THIRD = :ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL) AND" +
            "        ( V_US.STATE= :STATE OR :STATE IS NULL) AND" +
            "        ( V_US.CREATION_USER = :CREATION_USER OR :CREATION_USER IS NULL) AND" +
            "        ( V_US.MODIFY_USER = :MODIFY_USER OR :MODIFY_USER IS NULL)")
    List<UserThird> read(@Bind("ID_USER_THIRD") Long id_user_third, @Bind("UUID") String UUID,
                         @Bind("ID_PERSON") Long id_person,
                         @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE") Integer state,
                         @Bind("CREATION_USER") Date creation_user, @Bind("MODIFY_USER")Date modify_user);

    /**
     *
     * @param idUser
     * @param utDTO
     * @return
     */
    @SqlUpdate("UPDATE USER_THIRD SET " +
            "UUID = :utDTO.UUID " +
            "WHERE " +
            "ID_USER_THIRD = :idUser")
    int update(@Bind("idUser") Long idUser, @BindBean("utDTO")UserThirdDTO utDTO);

    /**
     *
     * @param idUser
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM V_USER WHERE ID_USER_THIRD = :idUser")
    List<Long> delete(@Bind("idUser") Long idUser);

    /**
     *
     * @param userThird
     * @return
     */
    @SqlUpdate("DELETE FROM USER_THIRD WHERE(" +
            "ID_PERSON = :userThird.id_person OR" +
            "ID_PERSON = :userThird.id_common_third OR" +
            "ID_PERSON = :userThird.uuid OR" +
            "ID_PERSON = :userThird.id_user_third OR" +
            ")")
    int permanentDelete(@BindBean UserThird userThird);

    // get PK
    @SqlQuery("SELECT ID_USER_THIRD \"LAST_ID\" FROM USER_THIRD WHERE ID_USER_THIRD IN (SELECT MAX(ID_USER_THIRD) FROM USER_THIRD)")
    Long getPkLast();
}