package com.name.business.DAOs;

import com.name.business.entities.ThirdLocation;
import com.name.business.mappers.ThirdLocationMapper;
import com.name.business.representations.ThirdLocationDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(ThirdLocationMapper.class)
public interface ThirdLocationDAO {

    @SqlUpdate("INSERT INTO THIRD_LOCATION " +
            "( ID_THIRD, ID_LOCATION, ID_COMMON_THIRD) VALUES" +
            "(:thloc.id_third, " +
            ":thloc.id_location, :id_common_third )")
    int create(@Bind("id_common_third") Long id_common_third,@BindBean("thloc") ThirdLocationDTO thirdLocationDTO);

    /**
     *
     * @param id_third_location
     * @param id_third
     * @param id_location
     * @param id_common_third
     * @param state
     * @param creation_third_location
     * @param modify_third_location
     * @return
     */
    @SqlQuery("SELECT * FROM V_THIRD_LOCATION  V_TH_LOC " +
            "WHERE (V_TH_LOC.ID_THIRD_LOCATION =:ID_THIRD_LOCATION OR :ID_THIRD_LOCATION IS NULL ) AND " +
            "      (V_TH_LOC.ID_THIRD =:ID_THIRD OR :ID_THIRD IS NULL ) AND " +
            "      (V_TH_LOC.ID_LOCATION =:ID_LOCATION OR :ID_LOCATION IS NULL ) AND " +
            "      (V_TH_LOC.ID_COMMON_THIRD=:ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL ) AND " +
            "      (V_TH_LOC.STATE=:STATE OR :STATE IS NULL ) AND " +
            "      (V_TH_LOC.CREATION_TH_LOC =:CREATION_TH_LOC OR :CREATION_TH_LOC IS NULL ) AND " +
            "      (V_TH_LOC.MODIFY_TH_LOC =:MODIFY_TH_LOC OR :MODIFY_TH_LOC IS NULL )")
    List<ThirdLocation> read(@Bind("ID_THIRD_LOCATION")Long id_third_location, @Bind("ID_THIRD")Long id_third,
                             @Bind("ID_LOCATION")Long id_location, @Bind("ID_COMMON_THIRD")Long id_common_third,
                             @Bind("STATE")Integer state, @Bind("CREATION_TH_LOC")Date creation_third_location,
                             @Bind("MODIFY_TH_LOC")Date modify_third_location);


    /**
     *
     * @param idThirdLocation
     * @param thirdLocationDTO
     * @return
     */
    @SqlUpdate("UPDATE THIRD_LOCATION SET " +
            "ID_LOCATION = :thirdLocationDTO.id_location, " +
            "ID_THIRD = :thirdLocationDTO.id_third " +
            "WHERE " +
            "ID_THIRD_LOCATION = :idThirdLocation")
    int update(@Bind("idThirdLocation") Long idThirdLocation, @BindBean("thirdLocationDTO") ThirdLocationDTO thirdLocationDTO);

    /**
     *
     * @param idThirdLocation
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM THIRD_LOCATION WHERE ID_THIRD_LOCATION = :idThirdLocation")
    List<Long> delete(@Bind("idThirdLocation") Long idThirdLocation);


    /**
     *
     * @param thirdLocation
     * @return
     */
    @SqlUpdate("DELETE FROM THIRD_LOCATION WHERE(" +
            "ID_COMMON_THIRD = :thirdLocation.id_common_third OR" +
            "ID_LOCATION = :thirdLocation.id_location OR" +
            "ID_THIRD = :thirdLocation.id_third OR" +
            "ID_THIRD_LOCATION = :thirdLocation.id_third_location OR" +
            ")")
    int permanentDelete(@BindBean ThirdLocation thirdLocation);

    // get PK
    @SqlQuery("SELECT ID_THIRD_LOCATION \"LAST_ID\" FROM THIRD_LOCATION WHERE ID_THIRD_LOCATION IN (SELECT MAX(ID_THIRD_LOCATION) FROM THIRD_LOCATION)")
    Long getPkLast();
}
