package com.name.business.DAOs;


import com.name.business.entities.ThirdType;
import com.name.business.mappers.ThirdTypeMapper;
import com.name.business.representations.ThirdTypeDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;


@RegisterMapper(ThirdTypeMapper.class)
public interface ThirdTypeDAO {

    /**
     *
     * @param thirdTypeDTO
     * @param id_common_third
     * @return
     */
    @SqlUpdate("INSERT INTO THIRD_TYPE " +
            "( NAME, ID_COMMON_THIRD) VALUES" +
            "(:th_type.name, :id_common_third " +
            ")")
    int create(@BindBean("th_type") ThirdTypeDTO thirdTypeDTO, @Bind("id_common_third") Long id_common_third);

    /**
     *
     * @param ID_THIRD_TYPE
     * @param name
     * @param ID_COMMON_THIRD
     * @param STATE
     * @return
     */
    @SqlQuery(" SELECT * FROM V_THIRD_TYPE " +
            "  WHERE (ID_THIRD_TYPE=:ID_THIRD_TYPE OR :ID_THIRD_TYPE IS NULL) AND " +
            "        (NAME LIKE :NAME OR :NAME IS NULL) AND " +
            "        (ID_COMMON_THIRD=:ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL) AND " +
            "        (STATE=:STATE OR :STATE IS NULL) ")
    List<ThirdType> read(@Bind("ID_THIRD_TYPE") Long ID_THIRD_TYPE, @Bind("NAME") String name, @Bind("ID_COMMON_THIRD") Long ID_COMMON_THIRD, @Bind("STATE") Integer STATE );

    /**
     *
     * @param idThird
     * @param ttDTO
     * @return
     */
    @SqlUpdate("UPDATE THIRD_TYPE SET " +
            "NAME = :thirdType.name " +
            "WHERE " +
            "ID_COMMON_THIRD = :idThird")
    int update(@Bind("idThird") Long idThird, @BindBean("thirdType") ThirdTypeDTO ttDTO);

    /**
     *
     * @param idThird
     * @return
     */
    @SqlQuery("SELECT ID_COMMON THIRD FROM THIRD_TYPE WHERE" +
            "ID_THIRD_TYPE = :idThird")
    List<Long> delete(@Bind("idThird") Long idThird);

    /**
     *
     * @param thirdType
     * @return
     */
    @SqlUpdate("DELETE FROM THIRD_TYPE WHERE(" +
            "ID_COMMON_THIRD = :thirdType.id_common_third OR" +
            "NAME = :thirdType.name OR" +
            "ID_THIRD_TYPE = :thirdType.id_third_type" +
            ")")
    int permanentDelete(@BindBean ThirdType thirdType);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_THIRD_TYPE \"LAST_ID\" FROM THIRD_TYPE WHERE ID_THIRD_TYPE IN (SELECT MAX(ID_THIRD_TYPE) FROM THIRD_TYPE)")
    Long getPkLast();
}