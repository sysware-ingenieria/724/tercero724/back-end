package com.name.business.DAOs;

import com.name.business.entities.Phone;
import com.name.business.mappers.PhoneMapper;
import com.name.business.representations.PhoneDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.Date;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(PhoneMapper.class)
@UseStringTemplate3StatementLocator
public interface PhoneDAO {

    /**
     *
     * @param phone
     * @return
     */
    @SqlUpdate("INSERT INTO PHONE " +
            "(ID_PHONE, PHONE, PRIORITY, ID_DIRECTORY) VALUES" +
            "(:phone.id_phone, :phone.phone, :phone.priority, :phone.id_directory)")
    int create(@BindBean Phone phone);


    /**
     *
     * @param id_phone
     * @param phone
     * @param priority
     * @param id_directory
     * @param id_common_third
     * @param state
     * @param creation_phone
     * @param modify_phone
     * @return
     */
    @SqlQuery("SELECT * FROM V_PHONE " +
            "  WHERE ( ID_PHONE = :ID_PHONE OR :ID_PHONE IS NULL ) AND " +
            "        ( PHONE = :PHONE OR :PHONE IS NULL ) AND " +
            "        ( PRIORITY = :PRIORITY OR :PRIORITY IS NULL ) AND " +
            "        ( ID_DIRECTORY = :ID_DIRECTORY OR :ID_DIRECTORY IS NULL ) AND " +
            "        ( ID_COMMON_THIRD = :ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL ) AND " +
            "        ( STATE = :STATE OR :STATE IS NULL ) AND " +
            "        ( CREATION_PHONE = :CREATION_PHONE OR :CREATION_PHONE IS NULL )AND " +
            "        ( MODIFY_PHONE = :MODIFY_PHONE OR :MODIFY_PHONE IS NULL )")
    List<Phone> read(@Bind("ID_PHONE")Long id_phone, @Bind("PHONE")String phone, @Bind("PRIORITY")Integer priority, @Bind("ID_DIRECTORY") Long id_directory,
                     @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE") Integer state,
                     @Bind("CREATION_PHONE") Date creation_phone, @Bind("MODIFY_PHONE") Date modify_phone);

    /**
     *
     * @param idPhone
     * @param phoneDTO
     * @return
     */
    @SqlUpdate("UPDATE PHONE SET " +
            "PHONE =  :phoneDTO.phone, " +
            "PRIORITY = :phoneDTO.priority " +
            //"ID_DIRECTORY = :phoneDTO.id_directory " +
            "WHERE " +
            "ID_PHONE =  :idPhone")
    int update(@Bind("idPhone") Long idPhone, @BindBean("phoneDTO") PhoneDTO phoneDTO);

    /**
     *
     * @param idPhone
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM V_PHONE WHERE ID_PHONE = :idPhone")
    List<Long> delete(@Bind("idPhone") Long idPhone);

    /**
     *
     * @param idPhone
     * @return
     */
    @SqlUpdate("DELETE FROM PHONE WHERE(" +
            "ID_PHONE =  :phone.id_phone OR" +
            "PHONE =  :phone.phone OR" +
            "PRIORITY = :phone.priority  OR" +
            "ID_DIRECTORY = :phone.id_directory" +
            ")")
    int permanenDelete(@Bind("idPhone") Long idPhone);


    // get PK
    @SqlQuery("SELECT ID_PHONE \"LAST_ID\" FROM PHONE WHERE ID_PHONE IN (SELECT MAX(ID_PHONE) FROM PHONE)")
    Long getPkLast();

    @SqlBatch("INSERT INTO PHONE " +
            "( PHONE, PRIORITY, ID_DIRECTORY) VALUES" +
            "( :contact, :priority, :id_directory)")
    void bulkPhone(@Bind("id_directory") Long id_directory,@Bind("contact") List<String> contact,@Bind("priority")List<Integer> priority);
}