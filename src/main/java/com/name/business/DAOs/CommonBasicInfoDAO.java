package com.name.business.DAOs;

import com.name.business.entities.CommonBasicInfo;
import com.name.business.entities.CommonBasicInfo;
import com.name.business.mappers.Common_BasicInfoMapper;
import com.name.business.representations.CommonBasicInfoDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(Common_BasicInfoMapper.class)
public interface CommonBasicInfoDAO {
    /**
     * @param basicInfoDTO
     * @return
     */
    @SqlUpdate("INSERT INTO COMMON_BASICINFO ( FULLNAME, ID_DOCUMENT_TYPE, DOCUMENT_NUMBER, IMG) " +
            "VALUES (:cbi.fullname,:cbi.id_document_type, :cbi.document_number, :cbi.img)")
    int create(@BindBean("cbi") CommonBasicInfoDTO basicInfoDTO);

    /**
     * @param id_common_basicinfo
     * @param fullname
     * @param id_documen_type
     * @param document_number
     * @param type_document
     * @param img
     * @return
     */
    @SqlQuery("SELECT * FROM V_COMMON_BASICINFO " +
            "WHERE (ID_COMMON_BASICINFO =:ID_COMMON_BASICINFO  OR :ID_COMMON_BASICINFO IS NULL ) AND " +
            "      (FULLNAME LIKE :FULLNAME OR :FULLNAME IS NULL ) AND " +
            "      (ID_DOCUMENT_TYPE = :ID_DOCUMENT_TYPE OR :ID_DOCUMENT_TYPE  IS NULL ) AND " +
            "      (DOCUMENT_NUMBER LIKE :DOCUMENT_NUMBER OR :DOCUMENT_NUMBER IS NULL) AND " +
            "      (TYPE_DOCUMENT LIKE  :TYPE_DOCUMENT OR :TYPE_DOCUMENT IS NULL) AND " +
            "      (IMG = :IMG OR  :IMG IS NULL ) ")
    List<CommonBasicInfo> read(@Bind("ID_COMMON_BASICINFO") Long id_common_basicinfo, @Bind("FULLNAME") String fullname,
                               @Bind("ID_DOCUMENT_TYPE") Long id_documen_type, @Bind("DOCUMENT_NUMBER") String document_number,
                               @Bind("TYPE_DOCUMENT") String type_document, @Bind("IMG") String img);

    /**
     *
     * @param idCommonBasicInfo
     * @param commonBasicInfoDTO
     * @return
     */
    @SqlUpdate("UPDATE COMMON_BASICINFO SET " +
            "FULLNAME = :commonBasicInfoDTO.fullname, " +
            "DOCUMENT_NUMBER = :commonBasicInfoDTO.document_number, " +
            "IMG = :commonBasicInfoDTO.img, " +
            "ID_DOCUMENT_TYPE = :commonBasicInfoDTO.id_document_type " +
            "WHERE ID_COMMON_BASICINFO = :idCommonBasicInfo")
    int update(@Bind("idCommonBasicInfo") Long idCommonBasicInfo, @BindBean("commonBasicInfoDTO") CommonBasicInfoDTO commonBasicInfoDTO);

    /**
     *
     * @param id_common_basicinfo
     * @param fullname
     * @param id_documen_type
     * @param document_number
     * @param img
     * @return
     */
    @SqlUpdate("DELETE FROM COMMON_BASICINFO " +
            "WHERE " +
            "  (ID_COMMON_BASICINFO = :ID_COMMON_BASICINFO OR :ID_COMMON_BASICINFO IS NULL ) AND " +
            "  (FULLNAME = :FULLNAME OR :FULLNAME IS NULL ) AND " +
            "  (ID_DOCUMENT_TYPE = :ID_DOCUMENT_TYPE OR :ID_DOCUMENT_TYPE IS NULL ) AND " +
            "  (DOCUMENT_NUMBER = :DOCUMENT_NUMBER OR :DOCUMENT_NUMBER IS NULL ) AND " +
            "  (IMG = :IMG OR :IMG IS NULL )" )
    int permanentDelete(@Bind("ID_COMMON_BASICINFO") Long id_common_basicinfo,@Bind("FULLNAME") String fullname,
               @Bind("ID_DOCUMENT_TYPE") Long  id_documen_type,@Bind("DOCUMENT_NUMBER") String document_number,
               @Bind("IMG") String img);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_BASICINFO \"LAST_ID\" FROM COMMON_BASICINFO WHERE ID_COMMON_BASICINFO IN (SELECT MAX(ID_COMMON_BASICINFO) FROM COMMON_BASICINFO)")
    Long getPkLast();
}
