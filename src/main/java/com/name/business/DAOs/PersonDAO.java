package com.name.business.DAOs;

import com.name.business.entities.Person;
import com.name.business.mappers.PersonMapper;
import com.name.business.representations.PersonDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(PersonMapper.class)
public interface PersonDAO {
    /**
     *
     * @param person
     * @param id_directory
     * @param id_common_third
     * @param id_common_basicinfo
     * @return
     */
    @SqlUpdate("INSERT INTO PERSON " +
            " ( SECOND_NAME, FIRST_LASTNAME, SECOND_LASTNAME, BIRTHDAY, " +
            " FIRST_NAME, ID_COMMON_BASICINFO, ID_COMMON_THIRD, ID_DIRECTORY) VALUES " +
            " ( :pe.second_name, :pe.first_lastname, :pe.second_lastname, " +
            " :pe.birthday, :pe.first_name, :id_common_basicinfo, :id_common_third, " +
            " :id_directory)")
    int create(@BindBean("pe") PersonDTO person, @Bind("id_directory") Long id_directory,@Bind("id_common_third") Long id_common_third,
               @Bind("id_common_basicinfo") Long id_common_basicinfo);

    /**
     *
     * @param id_person
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    @SqlQuery("SELECT * FROM V_PERSON V_PE " +
            "WHERE (V_PE.ID_PERSON =:ID_PERSON OR :ID_PERSON IS NULL ) AND" +
            "      (V_PE.FIRST_NAME LIKE :FIRST_NAME OR :FIRST_NAME IS NULL ) AND" +
            "      (V_PE.SECOND_NAME LIKE :SECOND_NAME OR :SECOND_NAME IS NULL ) AND" +
            "      (V_PE.FIRST_LASTNAME LIKE  :FIRST_LASTNAME OR :FIRST_LASTNAME IS NULL ) AND" +
            "      (V_PE.SECOND_LASTNAME LIKE  :SECOND_LASTNAME OR :SECOND_LASTNAME IS NULL ) AND" +
            "      (V_PE.BIRTHDAY =:BIRTHDAY OR :BIRTHDAY IS NULL ) AND" +
            "      (V_PE.ID_DIR_PERSON =:ID_DIR_PERSON OR :ID_DIR_PERSON IS  NULL ) AND" +
            "      (V_PE.ID_CBI_PERSON =:ID_CBI_PERSON OR :ID_CBI_PERSON IS NULL ) AND" +
            "      (V_PE.ID_DOCTYPE=:ID_DOCTYPE OR :ID_DOCTYPE IS NULL ) AND" +
            "      (V_PE.DOC_PERSON =:DOC_PERSON OR :DOC_PERSON IS NULL ) AND" +
            "      (V_PE.TYPEDOC_PPERSON = :TYPEDOC_PPERSON OR :TYPEDOC_PPERSON IS NULL ) AND" +
            "      (V_PE.FULLNAME_PERSON LIKE :FULLNAME_PERSON OR :FULLNAME_PERSON IS NULL ) AND" +
            "      (V_PE.IMG_PERSON =:IMG_PERSON OR :IMG_PERSON IS NULL ) AND" +
            "      (V_PE.ID_C_TH_PERSON =:ID_C_TH_PERSON OR :ID_C_TH_PERSON IS NULL ) AND" +
            "      (V_PE.STATE_PERSON =:STATE_PERSON OR :STATE_PERSON IS NULL ) AND" +
            "      (V_PE.CREATION_PERSON =:CREATION_PERSON OR :CREATION_PERSON IS NULL ) AND" +
            "      (V_PE.MODIFY_PERSON =:MODIFY_PERSON OR :MODIFY_PERSON IS NULL )")
    List<Person> read(@Bind("ID_PERSON") Long id_person,@Bind("FIRST_NAME") String first_name,
                      @Bind("SECOND_NAME") String second_name,@Bind("FIRST_LASTNAME") String first_lastname,
                      @Bind("SECOND_LASTNAME") String second_lastname,@Bind("BIRTHDAY") Date birthday,
                      @Bind("ID_DIR_PERSON") Long id_dir_person, @Bind("ID_CBI_PERSON") Long id_cbi_person,
                      @Bind("ID_DOCTYPE") Long id_doctype_person,@Bind("DOC_PERSON") String doc_person,
                      @Bind("TYPEDOC_PPERSON") Long typedoc_person,@Bind("FULLNAME_PERSON") String fullname_person,
                      @Bind("IMG_PERSON") String img_person,
                      @Bind("ID_C_TH_PERSON") Long id_c_th_person,@Bind("STATE_PERSON") Integer state_person,
                      @Bind("CREATION_PERSON") Date creation_person,@Bind("MODIFY_PERSON") Date modify_person);


    @SqlUpdate("UPDATE PERSON SET " +
            "SECOND_NAME = :personDTO.second_name, " +
            "FIRST_LASTNAME = :personDTO.first_lastname, " +
            "SECOND_LASTNAME = :personDTO.second_lastname, " +
            "BIRTHDAY = :personDTO.birthday, " +
            "FIRST_NAME = :personDTO.first_name, " +
            "ID_DIRECTORY =:personDTO.id_directory  " +
            "WHERE " +
            "ID_PERSON =  :idPerson")
    int update(@Bind("idPerson") Long idPerson, @BindBean("personDTO") PersonDTO personDTO);

    /**
     *
     * @param idPerson
     * @return
     */
    @SqlQuery("SELECT ID_C_TH_PERSON FROM V_PERSON " +
               "WHERE ID_PERSON = :idPerson")
    List<Long> delete(@Bind("idPerson") Long idPerson);

    /**
     *
     * @param idPerson
     * @return
     */
    @SqlQuery("SELECT ID_CBI_PERSON FROM V_PERSON " +
            "WHERE ID_PERSON = :idPerson")
    List<Long> deleteCommonBasicInfo(@Bind("idPerson") Long idPerson);

    /**
     *
     * @param idPerson
     * @return
     */
    @SqlQuery("SELECT ID_DIR_PERSON FROM V_PERSON " +
            "WHERE ID_PERSON = :idPerson")
    List<Long> deleteDirectory(@Bind("idPerson") Long idPerson);

    /**
     *
     * @param person
     * @param id_person
     * @param id_directory
     * @param id_common_third
     * @param id_common_basicinfo
     * @return
     */
    @SqlUpdate("DELETE FROM PERSON " +
            "WHERE( ID_PERSON =  :id_person  OR :id_person IS NULL ) AND " +
            "     ( SECOND_NAME = :pe.second_name   OR :pe.second_name IS NULL ) AND  " +
            "     ( FIRST_LASTNAME = :pe.first_lastname OR :pe.first_lastname IS NULL ) AND " +
            "     ( SECOND_LASTNAME = :pe.second_lastname OR :pe.second_lastname IS NULL ) AND " +
            "     ( BIRTHDAY = :pe.birthday   OR :pe.birthday IS NULL ) AND " +
            "     ( FIRST_NAME = :pe.first_name   OR  :pe.first_name IS NULL ) AND " +
            "     ( ID_COMMON_BASICINFO = :id_common_basicinfo   OR :id_common_basicinfo IS NULL ) AND " +
            "     ( ID_COMMON_THIRD = :id_common_third  OR :id_common_third IS NULL ) AND  " +
            "     ( ID_DIRECTORY =:id_directory OR :id_directory IS NULL ) ")
    int permanentDelete(@BindBean("pe") PersonDTO person,@Bind("id_person") Long id_person, @Bind("id_directory") Long id_directory,@Bind("id_common_third") Long id_common_third,
               @Bind("id_common_basicinfo") Long id_common_basicinfo);


    // get PK
    @SqlQuery("SELECT ID_PERSON \"LAST_ID\" FROM PERSON WHERE ID_PERSON IN (SELECT MAX(ID_PERSON) FROM PERSON)")
    Long getPkLast();
}

