package com.name.business.DAOs;

import com.name.business.entities.Location;
import com.name.business.mappers.LocationMapper;
import com.name.business.representations.Common_ThirdDTO;
import com.name.business.representations.DirectoryDTO;
import com.name.business.representations.LocationDTO;
import com.name.business.representations.ThirdDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(LocationMapper.class)
public interface LocationDAO {
    /**
     *
     * @param id_common_third
     * @param loc
     * @return
     */
    @SqlUpdate("INSERT INTO LOCATION " +
            "(ID_THIRD, ID_DIRECTORY, ID_COMMON_THIRD) VALUES" +
            "(:loc.id_third, :loc.id_directory, :id_common_third)")
    int create(@Bind("id_common_third") Long id_common_third,@BindBean("loc") LocationDTO loc);

    /**
     *
     * @param id_location
     * @param id_third
     * @param id_directory
     * @param id_common_third
     * @param state
     * @param creation_location
     * @param modify_location
     * @return
     */
    @SqlQuery("SELECT * FROM V_LOCATION " +
            "    WHERE ( ID_LOCATION = :ID_LOCATION OR :ID_LOCATION IS NULL ) AND" +
            "      ( ID_THIRD = :ID_THIRD OR :ID_THIRD IS NULL ) AND " +
            "      ( ID_DIRECTORY = :ID_DIRECTORY OR :ID_DIRECTORY IS NULL ) AND" +
            "      ( ID_COMMON_THIRD = :ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL) AND" +
            "      ( STATE= :STATE OR :STATE IS NULL) AND" +
            "      ( CREATION_LOCATION = :CREATION_LOCATION OR :CREATION_LOCATION IS NULL) AND" +
            "      ( MODIFY__LOCATION = :MODIFY__LOCATION OR :MODIFY__LOCATION IS NULL)")
    List<Location> read(@Bind("ID_LOCATION")Long id_location, @Bind("ID_THIRD")Long id_third, @Bind("ID_DIRECTORY")Long id_directory,
                        @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE") Integer state,
                        @Bind("CREATION_LOCATION") Date creation_location, @Bind("MODIFY__LOCATION")Date modify_location);

    /**
     *
     * @param idLocation
     * @param locationDTO
     * @return
     */
    @SqlUpdate("UPDATE LOCATION SET " +
            "ID_THIRD = :locationDTO.id_third, " +
            "ID_DIRECTORY = :locationDTO.id_directory " +
            "WHERE " +
            "ID_LOCATION = :idLocation")
    int update(@Bind("idLocation") Long idLocation, @BindBean("locationDTO") LocationDTO locationDTO);

    /**
     *
     * @param idLocation
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM LOCATION WHERE ID_LOCATION = :idLocation")
    List<Long> delete(@Bind("idLocation") Long idLocation);;

    /**
     *
     * @param idLocation
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE (SELECT " +
            "CTH.MODIFY_DATE MODIFY_DATE_LOC, " +
            "CTH.STATE STATE_LOC, " +
            "FROM LOCATION LOC, " +
            "INNER JOIN COMMON_THIRD CTH ON " +
            "LOC.ID_COMMON_THIRD = CTH.ID_COMMON_THIRD AND " +
            "LOC.ID_LOCATION = :idLoc " +
            ") UP SET " +
            "MODIFY_DATE_LOC = :dateModify, " +
            "STATE_LOC = :0 ")
    int delete(@Bind("idLoc") Long idLocation,
               @Bind("dateModify") Date date);


    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_LOCATION \"LAST_ID\" FROM LOCATION WHERE " +
            "ID_LOCATION IN (SELECT MAX(ID_LOCATION) FROM LOCATION)")
    Long getPkLast();
}
