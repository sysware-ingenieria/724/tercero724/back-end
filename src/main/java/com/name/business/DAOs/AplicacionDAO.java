package com.name.business.DAOs;


import com.name.business.entities.Aplicacion;
import com.name.business.mappers.AplicacionMapper;
import com.name.business.representations.AplicacionDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(AplicacionMapper.class)
public interface AplicacionDAO {

    @SqlQuery("SELECT ID_APPLICATION FROM APPLICATION WHERE ID_APPLICATION IN (SELECT MAX( ID_APPLICATION ) FROM APPLICATION ) ")
    Long getPkLast();

    @SqlUpdate("INSERT INTO  APPLICATION (ID_USER_APP,NAME ,DESCRIPTION,VERSION,KEY_APPLICATION,KEY_SERVER) VALUES (:id_usuario_app,:ap.nombre,:ap.descripcion,:ap.version, :key_aplicacion,:key_servidor)")
    long CREAR_APLICACION(@Bind("id_usuario_app") Long id_usuario_app, @Bind("key_aplicacion") String key_aplicacion, @Bind("key_servidor") String key_servidor, @BindBean("ap") AplicacionDTO aplicacionDTO);


    @SqlQuery("SELECT * FROM  APPLICATION  app WHERE\n" +
            "            (app.ID_APPLICATION=:id_aplicacion or :id_aplicacion IS NULL ) AND\n" +
            "            (app.ID_USER_APP=:id_usuario_app or :id_usuario_app IS NULL ) AND\n" +
            "            (app.NAME LIKE  :nombre or :nombre IS NULL ) AND\n" +
            "            (app.DESCRIPTION LIKE  :descripcion or :descripcion IS NULL ) AND\n" +
            "            (app.version =  :version or :version IS NULL ) AND\n" +
            "            (app.CREATION_DATE LIKE  :fecha_creacion or :fecha_creacion IS NULL ) AND\n" +
            "            (app.UPDATE_DATE LIKE  :fecha_actualizacion or :fecha_actualizacion IS NULL ) AND\n" +
            "            (app.KEY_APPLICATION =  :key_aplicacion or :key_aplicacion IS NULL ) AND\n" +
            "            (app.KEY_SERVER =  :key_servidor or :key_servidor IS NULL )")
    List<Aplicacion> OBTENER_APLICACIONES(@Bind("id_aplicacion") Long id_applicacion,
                                          @Bind("id_usuario_app") Long id_usuario_app, @Bind("nombre") String nombre,
                                          @Bind("descripcion") String descripcion, @Bind("version") String version,
                                          @Bind("fecha_creacion") String fecha_creacion, @Bind("fecha_actualizacion") String fecha_actualizacion,
                                          @Bind("key_aplicacion") String key_aplicacion, @Bind("key_servidor") String key_servidor);


    @SqlQuery(" SELECT count(ID_APPLICATION) FROM APPLICATION APP   WHERE  (APP.ID_USER_APP=:id_usuario AND  APP.NAME=:nombre)")
   Integer VERIFICAR_APLICACION_USUARIO(@Bind("id_usuario") Long id_usuario_app, @Bind("nombre") String nombre);

    @SqlUpdate("UPDATE APPLICATION SET NAME=:ap.nombre, DESCRIPTION=:ap.descripcion, VERSION=:ap.version  WHERE  (ID_APPLICATION=:id_aplicacion AND  ID_USER_APP=:id_usuario_app) ")
    int MODIFICAR_APLICACION(@Bind("id_aplicacion") Long id_aplicacion, @Bind("id_usuario_app") Long id_usuario_app, @BindBean("ap") AplicacionDTO aplicacionDTO);
}
