package com.name.business.DAOs;

import com.name.business.entities.CommonThird;
import com.name.business.mappers.CommonThirdMapper;
import com.name.business.representations.Common_ThirdDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(CommonThirdMapper.class)
public interface CommonThirdDAO {
    /**
     *
     * @param commonThird
     * @return
     */
    @SqlUpdate("INSERT INTO COMMON_THIRD " +
            "(  STATE, CREATION_DATE, MODIFY_DATE ) " +
            " VALUES (:cthird.state, :cthird.creation_date, :cthird.modify_date)")
    int create(@BindBean("cthird") Common_ThirdDTO commonThird);

    /**
     *
     * @param ID_COMMON_THIRD
     * @param STATE
     * @param CREATION_DATE
     * @param MODIFY_DATE
     * @return
     */
    @SqlQuery("SELECT * FROM COMMON_THIRD " +
            " WHERE ( ID_COMMON_THIRD = :ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL) AND " +
            "       ( STATE =:STATE OR :STATE IS NULL ) AND " +
            "       ( CREATION_DATE = :CREATION_DATE OR :CREATION_DATE IS NULL ) AND " +
            "       ( MODIFY_DATE = :MODIFY_DATE OR :MODIFY_DATE IS NULL )")
    List<CommonThird> read(@Bind("ID_COMMON_THIRD") Long ID_COMMON_THIRD, @Bind("STATE") Integer STATE, @Bind("CREATION_DATE") Date CREATION_DATE, @Bind("MODIFY_DATE") Date MODIFY_DATE);

    /**
     *
     * @param id_common_third
     * @param commonThirdDTO
     * @return
     */
    @SqlUpdate("UPDATE COMMON_THIRD SET " +
            "    STATE =:cthird.state," +
            "    CREATION_DATE = :cthird.creation_date ," +
            "    MODIFY_DATE = :cthird.modify_date" +
            "  WHERE  (ID_COMMON_THIRD = :id_common_third)")
    int update(@Bind("id_common_third") Long id_common_third, @BindBean("cthird") Common_ThirdDTO commonThirdDTO);

    /**
     * Delete: Change State
     * @param id_common_third
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON_THIRD SET " +
            "    STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON_THIRD = :id_common_third)")
    int delete(@Bind("id_common_third") Long id_common_third, @Bind("modify_date") Date date);

    /**
     * Delete: Permanent!
     * @param id_common_third
     * @param commonThirdDTO
     * @return
     */
    @SqlUpdate("DELETE FROM COMMON_THIRD" +
            "WHERE ( STATE = :STATE OR :STATE IS NULL ) AND " +
            "      ( CREATION_DATE = :CREATION_DATE OR :CREATION_DATE IS NULL )AND " +
            "      ( MODIFY_DATE = :MODIFY_DATE OR :MODIFY_DATE IS NULL ) AND " +
            "      ( ID_COMMON_THIRD = :ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL )")
    int permanentDelete(@Bind("id_common_third") Long id_common_third, @BindBean("cthird") Common_ThirdDTO commonThirdDTO);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD \"LAST_ID\" FROM COMMON_THIRD WHERE ID_COMMON_THIRD IN (SELECT MAX(ID_COMMON_THIRD) FROM COMMON_THIRD)")
    Long getPkLast();
}
