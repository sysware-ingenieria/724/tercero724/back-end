package com.name.business.DAOs;



import com.name.business.entities.Token;
import com.name.business.mappers.TokenMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

import static com.name.business.utils.constans.querys.TokenQuery.*;


/**
 * Created by luis_ on 28/01/2017.
 */
@RegisterMapper(TokenMapper.class)
public interface TokenDAO {

    @SqlUpdate(INSERT_TOKEN+"(:key_token,now() )")
    @GetGeneratedKeys
    long CREATE_TOKEN(@Bind("key_token") String key_token);

    @SqlQuery(SELECT_TOKEN+":key_token")
    Token FIND_TOKEN(@Bind("key_token") String token_value);

    @SqlQuery(SELECT_URIS_BY_ROLE+" WHERE rp.id_role=:id_role AND mc.verb=:http_verb ")
    List<String> FIND_URIS_BY_ROLE(@Bind("id_role") long id_role, @Bind("http_verb") String http_verb);

    @SqlUpdate(DELETE_TOKEN_BY_KEY+" :key_token")
    Integer DELETE_TOKEN(@Bind("key_token") String key_token);
}
