package com.name.business.DAOs;

import com.name.business.entities.DocumentType;
import com.name.business.mappers.DocumentTypeMapper;
import com.name.business.representations.DocumentTypeDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(DocumentTypeMapper.class)
public interface DocumentTypeDAO {

    /**
     *
     * @param documentTypeDTO
     * @return
     */
    @SqlUpdate("INSERT INTO DOCUMENT_TYPE " +
            "(NAME, ID_COMMON_THIRD ) " +
            "VALUES (:documentTypeDTO.name, :idCommonThird)")
    int create(@BindBean("documentTypeDTO") DocumentTypeDTO documentTypeDTO, @Bind("idCommonThird") Long idCommonThird);

    /**
     *
     * @param id_document_type
     * @param name
     * @return
     */
    @SqlQuery("SELECT * FROM V_DOCUMENT_TYPE  WHERE " +
            " (ID_DOCUMENT_TYPE = :ID_DOCUMENT_TYPE  OR :ID_DOCUMENT_TYPE IS NULL) AND " +
            " (NAME LIKE :NAME OR :NAME IS NULL) ")
    List<DocumentType> read(@Bind("ID_DOCUMENT_TYPE") Long id_document_type,@Bind("NAME") String name);

    /**
     *
     * @param idDocType
     * @return
     */
    @SqlUpdate("UPDATE DOCUMENT_TYPE SET " +
            "NAME = :dtDTO.name " +
            "WHERE " +
            "ID_DOCUMENT_TYPE = :idDocType")
    int update(@Bind("idDocType") Long idDocType, @BindBean("dtDTO") DocumentTypeDTO documentTypeDTO);

    /**
     *
     * @param idDocType
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM DOCUMENT_TYPE WHERE ID_DOCUMENT_TYPE = :idDocType")
    List<Long> delete(@Bind("idDocType") Long idDocType);

    /**
     *
     * @param documentType
     * @return
     */
    @SqlUpdate("DELETE FROM DOCUMENT_TYPE WHERE (" +
            "(NAME = :documentType.name OR :documentType.name) AND " +
            "ID_DOCUMENT_TYPE = :documentType.id_document_type OR :documentType.id_document_type" +
            ")")
    int permanentDelete(@BindBean DocumentTypeDTO documentType);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_DOCUMENT_TYPE \"LAST_ID\" FROM DOCUMENT_TYPE WHERE ID_DOCUMENT_TYPE IN (SELECT MAX(ID_DOCUMENT_TYPE) FROM DOCUMENT_TYPE)")
    Long getPkLast();
}