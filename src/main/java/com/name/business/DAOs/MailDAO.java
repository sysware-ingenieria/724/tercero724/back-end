package com.name.business.DAOs;

import com.name.business.entities.Mail;
import com.name.business.mappers.MailMapper;
import com.name.business.representations.MailDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(MailMapper.class)
public interface MailDAO {

        /**
         *
         * @param mail
         * @return
         */
        @SqlUpdate("INSERT INTO MAIL " +
                "(ID_MAIL, MAIL, PRIORITY, ID_DIRECTORY) VALUES" +
                "(:mail.id_mail, :mail.mail, :mail.priority, :mail.id_directory)")
        int create(@BindBean Mail mail);

        /**
         *
         * @param id_mail
         * @param mail
         * @param priority
         * @param id_directory
         * @return
         */
        @SqlQuery("SELECT * FROM V_MAIL " +
                "WHERE ( ID_MAIL = :ID_MAIL OR :ID_MAIL IS NULL ) AND " +
                "      ( MAIL = :MAIL OR :MAIL IS NULL ) AND " +
                "      ( PRIORITY = :PRIORITY OR :PRIORITY IS NULL ) AND " +
                "      ( ID_DIRECTORY = :ID_DIRECTORY OR :ID_DIRECTORY IS NULL ) AND " +
                "      ( ID_COMMON_THIRD = :ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL ) AND " +
                "      ( STATE = :STATE OR :STATE IS NULL ) AND " +
                "      ( CREATION_MAIL = :CREATION_MAIL OR :CREATION_MAIL IS NULL )AND " +
                "      ( MODIFY_MAIL = :MODIFY_MAIL OR :MODIFY_MAIL IS NULL )")
        List<Mail> read(@Bind("ID_MAIL")Long id_mail, @Bind("MAIL")String mail,
                        @Bind("PRIORITY")Integer priority, @Bind("ID_DIRECTORY") Long id_directory,
                        @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE") Integer state,
                        @Bind("CREATION_MAIL") Date creation, @Bind("MODIFY_MAIL") Date modify_mail);

        /**
         *
         * @param idMail
         * @param mailDTO
         * @return
         */
        @SqlUpdate("UPDATE MAIL SET " +
                "MAIL = :mailDTO.mail, " +
                "PRIORITY = :mailDTO.priority " +
                //"ID_DIRECTORY = :mailDTO.id_directory " + only if directoy is in v_mail
                "WHERE " +
                "ID_MAIL = :idMail")
        int update(@Bind("idMail") Long idMail, @BindBean("mailDTO") MailDTO mailDTO);

        /**
         *
         * @param idMail
         * @return
         */
        @SqlQuery("SELECT ID_COMMON_THIRD FROM V_MAIL WHERE ID_MAIL = :idMail")
        List<Long> delete(@Bind("idMail") Long idMail);

        /**
         *
         * @param mail
         * @return
         */
        @SqlUpdate("DELETE FROM MAIL WHERE(" +
                "ID_MAIL = :mail.id_mail OR" +
                "MAIL = :mail.mail OR " +
                "PRIORITY = :mail.priority OR" +
                "ID_DIRECTORY = :mail.id_directory" +
                ")")
        int permanenDelete(@BindBean Mail mail);

        // get PK
        @SqlQuery("SELECT ID_MAIL \"LAST_ID\" FROM MAIL WHERE ID_MAIL IN (SELECT MAX(ID_MAIL) FROM MAIL)")
        Long getPkLast();

        /**
         *
         * @param id_directory
         * @param contact
         * @param priority
         */
        @SqlBatch("INSERT INTO MAIL " +
                "( MAIL, PRIORITY, ID_DIRECTORY,ID_COMMON_THIRD) VALUES" +
                "( :contact, :priority, :id_directory,:id_common_third)")
        void bulkMail(@Bind("id_directory") Long id_directory, @Bind("contact") List<String> contact, @Bind("priority")List<Integer> priority,@Bind("id_common_third")List<Long> id_common_third);

}
