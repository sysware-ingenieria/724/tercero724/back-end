package com.name.business.DAOs;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Employee;
import com.name.business.representations.Common_ThirdDTO;
import com.name.business.representations.EmployeeDTO;
import org.skife.jdbi.v2.sqlobject.*;
import com.name.business.mappers.EmployeeMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

/**
 * Created by Niki on 17/07/2017.
 */
@RegisterMapper(EmployeeMapper.class)
public interface EmployeeDAO {

    /**
     *
     * @param employee
     * @param id_common_third
     * @return
     */
    @SqlUpdate("INSERT INTO EMPLOYEE (" +
            "ID_PERSON, SALARY, ID_COMMON_THIRD) VALUES" +
            "(:id_person,:employee.salary, :id_common_third)")
    int create(@BindBean("employee") EmployeeDTO employee, @Bind("id_person") Long id_person,@Bind("id_common_third") Long id_common_third);

    @SqlQuery("SELECT * FROM V_EMPLOYEE " +
            "WHERE ( ID_EMPLOYEE = :ID_EMPLOYEE OR :ID_EMPLOYEE IS NULL ) AND" +
            "      ( ID_PERSON = :ID_PERSON OR :ID_PERSON IS NULL ) AND" +
            "      ( SALARY = :SALARY OR :SALARY IS NULL ) AND" +
            "      ( ID_COMMON_THIRD = :ID_COMMON_THIRD OR :ID_COMMON_THIRD IS NULL ) AND" +
            "      ( STATE=:STATE OR :STATE IS NULL ) AND" +
            "      ( CREATION_EMPLOYEE=:CREATION_EMPLOYEE OR :CREATION_EMPLOYEE IS NULL ) AND " +
            "      ( MODIFY_EMPLOYEE=:MODIFY_EMPLOYEE OR :MODIFY_EMPLOYEE IS NULL )")
    List<Employee> read(@Bind("ID_EMPLOYEE")Long id_employee, @Bind("ID_PERSON")Long id_person,
                        @Bind("SALARY")Double salary,
                        @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE") Integer state,
                        @Bind("CREATION_EMPLOYEE") Date creation_employee, @Bind("MODIFY_EMPLOYEE")Date modify_employee);

    /**
     *
     * @param id_Employee
     * @param empDTO
     * @return
     */
    @SqlUpdate("UPDATE EMPLOYEE " +
            "SET " +
            "SALARY = :empDTO.salary " +
            "WHERE " +
            "ID_EMPLOYEE = :idEmp")
    int update(@Bind("idEmp") Long id_Employee, @BindBean("empDTO")EmployeeDTO empDTO);

    /***
     *
     * @param idEmployee
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM EMPLOYEE " +
            "WHERE ID_EMPLOYEE = :idEmployee")
    List<Long> delete(@Bind("idEmployee") Long idEmployee);

    /**
     *
     * @param employee
     * @return
     */
    @SqlUpdate("DELETE EMPLOYEE WHERE(" +
            "(ID_EMPLOYEE = :employee.idEmployee OR" +
            "ID_PERSON = :employee.idPerson OR" +
            "SALARY = :employee.Salary OR" +
            "ID_COMMON_THIRD = :idCommonThird" +
            ")")
    Long permanentDelete(@BindBean("employee") Employee employee);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_EMPLOYEE \"LAST_ID\" FROM EMPLOYEE WHERE ID_EMPLOYEE IN (SELECT MAX(ID_EMPLOYEE) FROM EMPLOYEE)")
    Long getPkLast();
}
