package com.name.business.DAOs;

import com.name.business.entities.Third;
import com.name.business.mappers.ThirdMapper;
import com.name.business.representations.ThirdDTO;
import com.name.business.representations.complex.ThirdCompleteDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(ThirdMapper.class)
public interface ThirdDAO {

    /**
     *
     * @param thirdDTO
     * @return
     */
    @SqlUpdate("INSERT INTO THIRD " +
            " ( ID_THIRD_TYPE, ID_COMMON_BASICINFO, " +
            " ID_THIRD_FATHER, ID_PERSON, ID_COMMON_THIRD) VALUES " +
            " (:th.id_third_type, :th.id_common_basicinfo, " +
            " :th.id_third_father, :th.id_person, :th.id_common_third)")
    int create(@BindBean("th") ThirdDTO thirdDTO);

    /**
     *
     * @param id_third
     * @param id_third_father
     * @param id_person
     * @param id_third_type
     * @param th_type_name
     * @param id_c_th_type
     * @param state_c_th_type
     * @param creation_c_th_type
     * @param modify_c_th_type
     * @param id_cb_third
     * @param id_typedoc_third
     * @param doc_third
     * @param typedoc_third
     * @param name_third
     * @param logo
     * @param id_common_third
     * @param state_third
     * @param creation_third
     * @param modify_third
     * @return
     */
    @SqlQuery("SELECT ID_THIRD, ID_THIRD_FATHER,ID_PERSON, ID_THIRD_TYPE,TH_TYPE_NAME,ID_C_TH_TYPE,STATE_C_TH_TYPE,CREATION_C_TH_TYPE,MODIFY_C_TH_TYPE, " +
            "  ID_CB_THIRD,ID_TYPEDOC_THIRD,DOC_THIRD,TYPEDOC_THIRD,NAME_THIRD,LOGO,ID_CTH_DOCT,STATE_DOCT,CREATION_DOCT,MODIFY_DOCT,ID_COMMON_THIRD,STATE_THIRD,CREATION_THIRD,MODIFY_THIRD " +
            " FROM V_THIRD V_T " +
            "WHERE (V_T.ID_THIRD=:ID_THIRD OR :ID_THIRD  IS NULL ) AND" +
            "      (V_T.ID_THIRD_FATHER=:ID_THIRD_FATHER OR :ID_THIRD_FATHER IS NULL ) AND" +
            "      (V_T.ID_PERSON=:ID_PERSON OR :ID_PERSON IS NULL ) AND" +
            "      (V_T.ID_THIRD_TYPE=:ID_THIRD_TYPE OR :ID_THIRD_TYPE IS NULL ) AND" +
            "      (V_T.TH_TYPE_NAME LIKE :TH_TYPE_NAME OR :TH_TYPE_NAME IS NULL ) AND" +
            "      (V_T.ID_C_TH_TYPE =:ID_C_TH_TYPE OR :ID_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.STATE_C_TH_TYPE =:STATE_C_TH_TYPE OR :STATE_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.CREATION_C_TH_TYPE=:CREATION_C_TH_TYPE OR :CREATION_C_TH_TYPE  IS NULL ) AND" +
            "      (V_T.MODIFY_C_TH_TYPE =:MODIFY_C_TH_TYPE  OR :MODIFY_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.ID_CB_THIRD = :ID_CB_THIRD OR :ID_CB_THIRD IS NULL ) AND" +
            "      (V_T.ID_TYPEDOC_THIRD=:ID_TYPEDOC_THIRD  OR :ID_TYPEDOC_THIRD IS NULL ) AND" +
            "      (V_T.DOC_THIRD=:DOC_THIRD OR :DOC_THIRD IS NULL ) AND" +
            "      (V_T.TYPEDOC_THIRD LIKE :TYPEDOC_THIRD OR :TYPEDOC_THIRD IS NULL ) AND" +
            "      (V_T.NAME_THIRD LIKE :NAME_THIRD OR :NAME_THIRD IS NULL ) AND" +
            "      (V_T.LOGO =:LOGO OR :LOGO IS NULL ) AND" +
            "      (V_T.ID_COMMON_THIRD =:ID_COMMON_THIRD OR :ID_COMMON_THIRD  IS NULL ) AND" +
            "      (V_T.STATE_THIRD =:STATE_THIRD OR :STATE_THIRD  IS NULL ) AND " +
            "      (V_T.CREATION_THIRD =:CREATION_THIRD OR :CREATION_THIRD IS NULL ) AND" +
            "      (V_T.MODIFY_THIRD=:MODIFY_THIRD OR :MODIFY_THIRD IS NULL )")
    List<Third> read(@Bind("ID_THIRD") Long id_third, @Bind("ID_THIRD_FATHER") Long id_third_father,@Bind("ID_PERSON") Long id_person, @Bind("ID_THIRD_TYPE") Long id_third_type, @Bind("TH_TYPE_NAME") String th_type_name, @Bind("ID_C_TH_TYPE")Long id_c_th_type,
                     @Bind("STATE_C_TH_TYPE") Integer state_c_th_type, @Bind("CREATION_C_TH_TYPE") Date creation_c_th_type , @Bind("MODIFY_C_TH_TYPE") Date modify_c_th_type,
                     @Bind("ID_CB_THIRD") Long id_cb_third, @Bind("ID_TYPEDOC_THIRD") Long id_typedoc_third, @Bind("DOC_THIRD") String doc_third, @Bind("TYPEDOC_THIRD") String typedoc_third, @Bind("NAME_THIRD") String name_third,
                     @Bind("LOGO")String logo, @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE_THIRD") Integer state_third, @Bind("CREATION_THIRD")Date creation_third , @Bind("MODIFY_THIRD")Date modify_third);

    /**
     *
     * @param idThird
     * @param third
     * @return
     */
    @SqlUpdate("UPDATE THIRD SET " +
            "ID_THIRD_TYPE = :thirdDTO.id_third_type, " +
            "ID_THIRD_FATHER = :thirdDTO.id_third_father, " +
            "WHERE " +
            "ID_THIRD = :idThird")
    int update(@Bind("idThird") Long idThird, @BindBean("thirdDTO") ThirdCompleteDTO third);

    /**
     *
     * @param idThird
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM THIRD WHERE ID_THIRD = :idThird")
    List<Long> delete(@Bind("idThird") Long idThird);

    /***
     *
     * @param id_third
     * @param third
     * @return
     */
    @SqlUpdate("DELETE FROM THIRD " +
            "WHERE " +
            "  ( ID_THIRD = :id_third OR :id_third IS NULL ) AND " +
            "  ( ID_THIRD_TYPE = :th.third_type OR :th.third_type IS NULL ) AND  " +
            "  ( ID_COMMON_BASICINFO = :th.id_common_basicinfo OR :th.id_common_basicinfo IS NULL ) AND " +
            "  ( ID_THIRD_FATHER = :th.id_third_father OR th.id_third_father IS NULL ) AND " +
            "  ( ID_PERSON = :th.id_person OR :th.id_person IS NULL ) AND " +
            "  ( ID_COMMON_THIRD =  :th.id_common_third OR :th.id_common_third IS NULL ) ")
    int delete(@Bind("id_third") Long id_third,@BindBean("th") ThirdDTO third);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_THIRD " +
            "FROM THIRD " +
            "WHERE ID_THIRD IN (SELECT MAX(ID_THIRD) FROM THIRD)")
    Long getPkLast();
}
