package com.name.business.DAOs;

import com.name.business.entities.Directory;
import com.name.business.mappers.DirectoryMapper;
import com.name.business.representations.Common_ThirdDTO;
import com.name.business.representations.DirectoryDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(DirectoryMapper.class)
public interface DirectoryDAO {


    @SqlUpdate("INSERT INTO DIRECTORY " +
            "(ADDRESS, COUNTRY, CITY, WEBPAGE, ID_COMMON_THIRD) " +
            "VALUES (:address, :country, :city, :webpage, :id_common_third)")
    int create(@Bind("id_common_third") Long id_common_third,@Bind("address") String address,
               @Bind("country") String country,@Bind("city")  String city, @Bind("webpage")  String webpage);

    @SqlQuery("SELECT * FROM V_DIRECTORY V_DIR " +
            "WHERE " +
            "  ( V_DIR.ID_DIRECTORY = :ID_DIRECTORY OR :ID_DIRECTORY IS NULL ) AND " +
            "  ( V_DIR.ADDRESS LIKE :ADDRESS OR :ADDRESS IS NULL ) AND " +
            "  ( V_DIR.COUNTRY LIKE :COUNTRY OR :COUNTRY IS NULL ) AND " +
            "  ( V_DIR.CITY LIKE  :CITY OR :CITY IS  NULL ) AND " +
            "  ( V_DIR.WEBPAGE LIKE :WEBPAGE OR :WEBPAGE IS NULL ) AND " +
            "  ( V_DIR.ID_CT_DIRECTORY = :ID_CT_DIRECTORY OR :ID_CT_DIRECTORY IS NULL ) AND " +
            "  ( V_DIR.STATE_DIR =:STATE_DIR OR :STATE_DIR IS  NULL ) AND " +
            "  ( V_DIR.CREATION_DIR =:CREATION_DIR OR :CREATION_DIR IS NULL )AND " +
            "  ( V_DIR.MODIFY_DIR=:MODIFY_DIR OR :MODIFY_DIR IS NULL )")
    List<Directory> read(@Bind("ID_DIRECTORY")Long id_direcotory,@Bind("ADDRESS")String address,
                         @Bind("COUNTRY")String country,@Bind("CITY")String city,
                         @Bind("WEBPAGE")String webpage,@Bind("ID_CT_DIRECTORY")Long id_ct_directory,
                         @Bind("STATE_DIR")Integer state_dir,@Bind("CREATION_DIR")Date creation_dir,
                         @Bind("MODIFY_DIR") Date modify);

    /**
     *
     * @param idDirectory
     * @param directoryDTO
     * @return
     */
    @SqlUpdate("UPDATE DIRECTORY SET " +
            "ADDRESS = :directoryDTO.address, " +
            "COUNTRY = :directoryDTO.country, " +
            "CITY = :directoryDTO.city, " +
            "WEBPAGE = :directoryDTO.webpage " +
            "WHERE ID_DIRECTORY = :idDirectory")
    int update(@Bind("idDirectory") Long idDirectory, @BindBean("directoryDTO") DirectoryDTO directoryDTO);
    
    /**
     * 
     * @param idDirectory
     * @return
     */
    @SqlQuery("SELECT ID_CT_DIRECTORY FROM V_DIRECTORY WHERE ID_DIRECTORY = :idDirectory")
    List<Long> delete(@Bind("idDirectory") Long idDirectory);

    /**
     *
     * @param directory
     * @return
     */
    @SqlUpdate("DELETE FROM DIRECTORY WHERE (" +
            "ID_DIRECTORY = (:directory.idDirectory) OR" +
            "ADDRESS = (:directory.address) OR" +
            "COUNTRY = (:directory.country) OR" +
            "CITY = (:directory.city) OR" +
            "WEBPAGE = (:directory.modifyDate) OR" +
            "ID_COMMON_THIRD = (:directory.idCommonThird) OR" +
            ")")
    int permanentDelete(@BindBean DirectoryDTO directory);

    /**
     *
     * @return LAST PK
     */
    @SqlQuery("SELECT ID_DIRECTORY \"LAST_ID\" FROM DIRECTORY WHERE ID_DIRECTORY IN (SELECT MAX(ID_DIRECTORY) FROM DIRECTORY)")
    Long getPkLast();
}
