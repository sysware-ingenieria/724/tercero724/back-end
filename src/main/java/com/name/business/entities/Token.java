package com.name.business.entities;

/**
 * Created by luis_ on 29/01/2017.
 */
public class Token {
    private long id_token;
    private final String KEY_TOKEN;
    private final long id_role;
    private final long id_person;
    private final long id_user;
    private final String username;
    private final String fullName;
    private final String lastName;

    public Token(long id_token, String KEY_TOKEN, long id_role, long id_person, long id_user, String username,
                 String fullName, String lastName) {
        this.id_token = id_token;
        this.KEY_TOKEN = KEY_TOKEN;
        this.id_role = id_role;
        this.id_person = id_person;
        this.id_user=id_user;
        this.username = username;
        this.fullName = fullName;
        this.lastName = lastName;
    }

    public long getId_user() {
        return id_user;
    }

    public long getId_token() {
        return id_token;
    }

    public void setId_token(long id_token) {
        this.id_token = id_token;
    }

    public String getKEY_TOKEN() {
        return KEY_TOKEN;
    }

    public long getId_role() {
        return id_role;
    }

    public long getId_person() {
        return id_person;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public String getLastName() {
        return lastName;
    }
}
