package com.name.business.entities;

import java.util.Date;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdType {

    private Long id_third_type;
    private Long id_common_third;
    private String name;
    private Integer state;
    private Date creation_date;
    private Date modify_date;

    public ThirdType(Long id_third_type, Long id_common_third, String name, CommonThird commonThird) {
        this.id_third_type = id_third_type;
        this.id_common_third = id_common_third;
        this.name = name;
        this.state = commonThird.getState();
        this.creation_date = commonThird.getCreate_date();
        this.modify_date = commonThird.getModify_date();
    }

    public Long getId_third_type() {
        return id_third_type;
    }

    public void setId_third_type(Long id_third_type) {
        this.id_third_type = id_third_type;
    }

    public Long getId_common_third() {
        return id_common_third;
    }

    public void setId_common_third(Long id_common_third) {
        this.id_common_third = id_common_third;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
