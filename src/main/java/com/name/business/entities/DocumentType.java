package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.representations.CommonBasicInfoDTO;

/**
 * Created by trossky on 14/07/17.
 */
public class DocumentType {

    private long id_document_type;
    private String name;
    private CommonThird commonThird;

    public DocumentType(long id_document_type, String name, CommonThird commonThird) {
        this.id_document_type = id_document_type;
        this.name = name;
        this.commonThird = commonThird;
    }

    public long getId_document_type() {
        return id_document_type;
    }

    public void setId_document_type(long id_document_type) {
        this.id_document_type = id_document_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommonThird getCommonThird() {
        return commonThird;
    }

    public void setCommonThird(CommonThird commonThird) {
        this.commonThird = commonThird;
    }
}
