package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class Employee {
    private Long id_employee;
    private Double salary;
    private CommonThird commonThird;

    public Employee(Long id_employee, Double salary, CommonThird commonThird) {
        this.id_employee = id_employee;
        this.salary = salary;
        this.commonThird = commonThird;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Long getId_employee() {
        return id_employee;
    }

    public void setId_employee(Long id_employee) {
        this.id_employee = id_employee;
    }

    public CommonThird getCommonThird() {
        return commonThird;
    }

    public void setCommonThird(CommonThird commonThird) {
        this.commonThird = commonThird;
    }
}
