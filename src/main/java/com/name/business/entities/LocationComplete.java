package com.name.business.entities;

import java.util.List;

public class LocationComplete {

    private Long id_location;
    private Long id_third;
    private List<Directory> directory;
    private CommonThird state;

    public LocationComplete(Long id_location, Long id_third, List<Directory> directory, CommonThird state) {
        this.id_location = id_location;
        this.id_third = id_third;
        this.directory = directory;
        this.state = state;
    }

    public Long getId_location() {
        return id_location;
    }

    public void setId_location(Long id_location) {
        this.id_location = id_location;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public List<Directory> getDirectory() {
        return directory;
    }

    public void setDirectory(List<Directory> directory) {
        this.directory = directory;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }
}
