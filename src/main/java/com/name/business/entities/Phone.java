package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class Phone {

    private Long id_phone;
    private Long id_directory;
    private String phone;
    private Integer priority;
    private CommonThird state;


    public Phone(Long id_phone, Long id_directory, String phone, Integer priority, CommonThird state) {
        this.id_phone = id_phone;
        this.id_directory = id_directory;
        this.phone = phone;
        this.priority = priority;
        this.state = state;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }

    public Long getId_phone() {
        return id_phone;
    }

    public void setId_phone(Long id_phone) {
        this.id_phone = id_phone;
    }

    public Long getId_directory() {
        return id_directory;
    }

    public void setId_directory(Long id_directory) {
        this.id_directory = id_directory;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
