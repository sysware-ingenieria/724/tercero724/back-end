package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class Location {
    private Long id_location;
    private Long id_third;
    private Long id_directory;
    private CommonThird state;

    public Location(Long id_location, Long id_third, Long id_directory, CommonThird state) {
        this.id_location = id_location;
        this.id_third = id_third;
        this.id_directory = id_directory;
        this.state = state;
    }

    public Long getId_location() {
        return id_location;
    }

    public void setId_location(Long id_location) {
        this.id_location = id_location;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_directory() {
        return id_directory;
    }

    public void setId_directory(Long id_directory) {
        this.id_directory = id_directory;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }
}
