package com.name.business.entities;

public class TokenEntity {
	
	private final String tokenValue;
	
	private final String username;
	
	private final String userRole;
	
	private final Integer idPerson;

	public TokenEntity(String tokenValue, String username, String userRole, Integer idPerson) {
		
		this.tokenValue = tokenValue;
		this.username = username;
		this.userRole = userRole;
		this.idPerson = idPerson;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public String getUsername() {
		return username;
	}

	public String getUserRole() {
		return userRole;
	}

	public Integer getIdPerson() {
		return idPerson;
	}

}
